﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TCC.ServiceCommerce.Commom.Extensions
{
    public static class EnumerableExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> list)
        {
            return list == null || !list.Any();
        }

        public static string Join<T>(this IEnumerable<T> list)
        {
            return Join(list, ", ");
        }

        public static string Join<T>(this IEnumerable<T> list, string separator)
        {
            return string.Join(separator, list);
        }

        public static string Join(this IEnumerable list)
        {
            return Join(list, ", ");
        }

        public static string Join(this IEnumerable list, string separator)
        {
            if (list == null)
            {
                return string.Empty;
            }

            IEnumerator en = list.GetEnumerator();

            if (!en.MoveNext())
            {
                return string.Empty;
            }

            StringBuilder result = new StringBuilder();

            if (en.Current != null)
            {
                result.Append(en.Current);
            }

            while (en.MoveNext())
            {
                result.Append(separator);

                if (en.Current != null)
                {
                    result.Append(en.Current);
                }
            }

            IDisposable disp = en as IDisposable;

            if (disp != null)
            {
                disp.Dispose();
            }

            return result.ToString();
        }

        public static void ForEach<T>(this IEnumerable<T> ie, Action<T> action)
        {
            if (ie != null)
            {
                foreach (var i in ie)
                {
                    action(i);
                }
            }
        }

        public static void AddIn<T>(this IEnumerable<T> source, ICollection<T> target)
        {
            if (target != null && source != null)
            {
                foreach (T item in source)
                    target.Add(item);
            }
        }
    }
}
