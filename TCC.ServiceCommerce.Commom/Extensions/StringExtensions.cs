﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;

namespace TCC.ServiceCommerce.Commom.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Retorna se a string indicada em s é nula ou vazia.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        /// <summary>
        /// Formata a string informada utilizando InvariantCulture.
        /// </summary>
        /// <param name="value">A string.</param>
        /// <param name="args">Os argumentos para formatação da string.</param>
        /// <returns>A string formatada.</returns>
        public static string With(this string value, params object[] args)
        {
            return string.Format(CultureInfo.InvariantCulture, value, args);
        }

        /// <summary>
        /// Converte string para um MD5Hash.
        /// </summary>
        /// <param name="text">Texto</param>
        /// <returns>Retorna string MD5Hash</returns>
        public static string MD5Hash(this string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return null;
            }

            var hash = new StringBuilder();

            using (var md5provider = new MD5CryptoServiceProvider())
            {
                byte[] bytes = md5provider.ComputeHash(Encoding.UTF8.GetBytes(text));

                for (int i = 0; i < bytes.Length; i++)
                {
                    hash.Append(bytes[i].ToString("x2"));
                }

                return hash.ToString();
            }
        }

        public static string EncodeBase64(this string str)
            => Convert.ToBase64String(Encoding.UTF8.GetBytes(str));

        public static string DecodeBase64(this string base64Str)
            => Encoding.UTF8.GetString(Convert.FromBase64String(base64Str));
    }
}
