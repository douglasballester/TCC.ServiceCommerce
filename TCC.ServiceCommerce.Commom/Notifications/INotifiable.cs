﻿using System.Collections.Generic;

namespace TCC.ServiceCommerce.Commom.Notifications
{
    public interface INotifiable
    {
        IReadOnlyCollection<Notification> Notifications { get; }

        void AddNotification(Notification notification);

        void AddNotifications(IEnumerable<Notification> notifications);

        bool IsValid();

        void ClearNotifications();
    }
}
