﻿using TCC.ServiceCommerce.Commom.Extensions;

namespace TCC.ServiceCommerce.Commom.Notifications
{
    public sealed class Notification : Notifiable
    {
        //public NotificationCulture NotificationCulture { get; private set; }

        public string Message { get; private set; }

        public Notification(string message)
        {
            Message = message;
        }

        public Notification(string code, string message)
        {
            //NotificationCulture = new NotificationCulture(code);
            Message = message;
        }

        public Notification(string code, string culture, string message)
        {
            //NotificationCulture = new NotificationCulture(code, culture);
            Message = message;
        }

        //public Notification Format(params object[] @params)
        //    => new Notification(NotificationCulture.Code, NotificationCulture.Culture, Message.With(@params));

        public override string ToString() => Message;
    }
}
