﻿using System;

namespace TCC.ServiceCommerce.Commom.Notifications
{
    public struct NotificationCulture : IEquatable<NotificationCulture>
    {
        public string Code { get; private set; }

        public string Culture { get; private set; }

        public NotificationCulture(string code, string culture)
        {
            if (string.IsNullOrEmpty(code))
                throw new ArgumentNullException(nameof(code));

            if (string.IsNullOrEmpty(culture))
                throw new ArgumentNullException(nameof(culture));

            Code = code;
            Culture = culture;
        }

        public NotificationCulture(string code)
        {
            if (string.IsNullOrEmpty(code))
                throw new ArgumentNullException(nameof(code));

            Code = code;
            Culture = "pt-BR";
        }

        public bool Equals(NotificationCulture other)
        {
            return string.Equals(other.Code, Code, StringComparison.InvariantCultureIgnoreCase) &&
                   string.Equals(other.Culture, Culture, StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (!(obj is NotificationCulture))
            {
                return false;
            }

            return Equals((NotificationCulture)obj);
        }

        public override int GetHashCode()
        {
            if (Code == null || Culture == null)
                return 0;

            return Code.GetHashCode() ^ Culture.GetHashCode();
        }

        public override string ToString() => string.Concat(Code, "-", Culture);

        public static implicit operator NotificationCulture(string code) => new NotificationCulture(code);
    }
}
