﻿using System.Collections.Generic;

namespace TCC.ServiceCommerce.Commom.Notifications
{
    public abstract class Notifiable : INotifiable
    {
        #region Fields
        private List<Notification> _notifications;
        #endregion

        #region Properties
        public IReadOnlyCollection<Notification> Notifications => InternalNotifications;

        private List<Notification> InternalNotifications
        {
            get
            {
                if (_notifications == null)
                {
                    _notifications = new List<Notification>();
                }

                return _notifications;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adiciona uma notificação na lista de notificações.
        /// </summary>
        /// <param name="notification"></param>
        public void AddNotification(Notification notification)
        {
            if (notification != null)
                InternalNotifications.Add(notification);
        }

        /// <summary>
        /// Adiciona uma notificação na lista de notificações.
        /// </summary>
        /// <param name="notifications"></param>
        public void AddNotifications(IEnumerable<Notification> notifications)
        {
            InternalNotifications.AddRange(notifications);
        }

        /// <summary>
        /// Limpa a lista de notificações.
        /// </summary>
        public void ClearNotifications()
        {
            _notifications = null;
        }

        /// <summary>
        /// Verifica se existe alguma notificação na lista de notificações.
        /// </summary>
        /// <returns>True caso exista notificação, falso caso contrário.</returns>
        public virtual bool IsValid()
        {
            return InternalNotifications.Count == 0;
        }
        #endregion
    }
}
