﻿using System.IO;
using System.Reflection;

namespace TCC.ServiceCommerce.Commom.Commons
{
    public static class EmbeddedResourceFile
    {
        #region Fields
        private static string _separatorAssembly = "_";
        private static string _resourceReplace = ".";
        private static Assembly _assembly;
        #endregion

        #region Methods        
        /// <summary>
        /// Realiza a leitura do comando no recurso informado.
        /// </summary>
        /// <param name="resourceName">O nome do resource.</param>
        /// <returns>Texto do resource.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public static string Read(string resourceName)
        {
            Stream stream = null;
            try
            {
                stream = ReadStream(resourceName);
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
            finally
            {
                if (stream != null)
                    stream.Dispose();
            }
        }

        /// <summary>
        /// Realiza a leitura do comando no recurso informado.
        /// </summary>
        /// <param name="resourceName">O nome do resource.</param>
        /// <returns>Stream do resource.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public static Stream ReadStream(string resourceName)
        {
            LoadAssembly(resourceName);
            return GetEmbeddedResource(resourceName);
        }

        private static Stream GetEmbeddedResource(string resourceName)
        {
            return _assembly.GetManifestResourceStream(resourceName.Replace(_separatorAssembly, _resourceReplace));
        }

        private static void LoadAssembly(string resourceName)
        {
            _assembly = Assembly.Load(resourceName.Split(_separatorAssembly.ToCharArray())[0]);
        }
        #endregion
    }
}
