﻿using System;
using System.Collections.Generic;

namespace TCC.ServiceCommerce.Commom.DI
{
    public interface IContainer
    {
        /// <summary>
        /// Obtém um serviço
        /// </summary>
        /// <typeparam name="T">Tipo do serviço</typeparam>
        /// <returns>O serviço</returns>
        T GetService<T>();

        /// <summary>
        /// Obtém um serviço
        /// </summary>
        /// <param name="serviceType">Tipo do serviço</param>
        /// <returns>O serviço</returns>
        object GetService(Type serviceType);

        /// <summary>
        /// Obtém uma lista de serviços
        /// </summary>
        /// <typeparam name="T">Tipo do serviço</typeparam>
        /// <returns>Os serviços</returns>
        IEnumerable<T> GetServices<T>();

        /// <summary>
        /// Obtém uma lista de serviços
        /// </summary>
        /// <param name="serviceType">Tipo do serviço</param>
        /// <returns>Os serviços</returns>
        IEnumerable<object> GetServices(Type serviceType);

        /// <summary>
        /// Inicia um novo escopo no container.
        /// </summary>
        void BeginScope();

        /// <summary>
        /// Finaliza o escopo atual do container.
        /// </summary>
        void EndCurrentScope();
    }
}
