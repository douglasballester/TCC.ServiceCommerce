﻿using TCC.ServiceCommerce.Commom.Notifications;

namespace TCC.ServiceCommerce.Commom.Rules
{
    public static class ValidationRules
    {
        public static GroupOfRules<TTarget> For<TTarget>(TTarget target) where TTarget : INotifiable
        {
            return new GroupOfRules<TTarget>(target);
        }
    }
}
