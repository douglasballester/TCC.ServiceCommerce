﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Commom.Notifications;

namespace TCC.ServiceCommerce.Commom.Rules
{
    public class GroupOfRules<TTarget> where TTarget : INotifiable
    {
        private readonly ICollection<IRule<TTarget>> _rules;
        private readonly TTarget _target;

        public GroupOfRules(TTarget target)
        {
            _rules = new List<IRule<TTarget>>();
            _target = target;
        }

        public GroupOfRules<TTarget> Add(IRule<TTarget> rule)
        {
            return Add(rule, x => true);
        }

        public GroupOfRules<TTarget> Add(IRule<TTarget> rule, System.Func<TTarget, bool> condition)
        {
            if (condition(_target))
            {
                _rules.Add(rule);
            }

            return this;
        }

        public void Run()
        {
            foreach (var item in _rules)
            {
                item.Validate(_target);
                if (!_target.IsValid())
                    break;
            }
        }
    }
}
