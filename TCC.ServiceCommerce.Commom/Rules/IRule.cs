﻿using TCC.ServiceCommerce.Commom.Notifications;

namespace TCC.ServiceCommerce.Commom.Rules
{
    public interface IRule<in TTarget> where TTarget : INotifiable
    {
        void Validate(TTarget target);
    }
}
