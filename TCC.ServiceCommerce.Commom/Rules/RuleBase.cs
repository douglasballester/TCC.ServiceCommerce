﻿using TCC.ServiceCommerce.Commom.DI;

namespace TCC.ServiceCommerce.Commom.Rules
{
    public abstract class RuleBase
    {
        protected TRepository GetRepository<TRepository>()
        {
            return DIContainer.Container.GetService<TRepository>();
        }
    }
}
