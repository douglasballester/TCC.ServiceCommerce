﻿using LightInject;
using System.Configuration;
using TCC.ServiceCommerce.Application.Cidades.CommandHandlers;
using TCC.ServiceCommerce.Application.Cidades.Commands.Inputs;
using TCC.ServiceCommerce.Application.CommandHandlersBase;
using TCC.ServiceCommerce.Application.Habilidades.CommandHandlers;
using TCC.ServiceCommerce.Application.Habilidades.Commands.Inputs;
using TCC.ServiceCommerce.Application.LevenshteinSearch.CommandHandlers;
using TCC.ServiceCommerce.Application.LevenshteinSearch.Commands;
using TCC.ServiceCommerce.Application.Perfis.CommandHandlers;
using TCC.ServiceCommerce.Application.Perfis.Commands.Inputs;
using TCC.ServiceCommerce.Application.Posts.CommandHandlers;
using TCC.ServiceCommerce.Application.Posts.Commands.Inputs;
using TCC.ServiceCommerce.Application.Recomendacoes.CommandHandlers;
using TCC.ServiceCommerce.Application.Recomendacoes.Commands;
using TCC.ServiceCommerce.Application.Usuarios.CommandHandlers;
using TCC.ServiceCommerce.Application.Usuarios.Commands.Inputs;
using TCC.ServiceCommerce.Domain.Cidades.Repositories;
using TCC.ServiceCommerce.Domain.Dicionarios.Repositories;
using TCC.ServiceCommerce.Domain.Habilidades.Repositories;
using TCC.ServiceCommerce.Domain.HistoricosContratacao.Repositories;
using TCC.ServiceCommerce.Domain.Mensagens.Repositories;
using TCC.ServiceCommerce.Domain.Perfis.Repositories;
using TCC.ServiceCommerce.Domain.Posts.Repositories;
using TCC.ServiceCommerce.Domain.Usuarios.Repositories;
using TCC.ServiceCommerce.Infrastructure.Cidades.Repositories;
using TCC.ServiceCommerce.Infrastructure.Data.RepositoriesBase.Context;
using TCC.ServiceCommerce.Infrastructure.Dicionarios.Repositories;
using TCC.ServiceCommerce.Infrastructure.Habilidades.Repositories;
using TCC.ServiceCommerce.Infrastructure.HistoricosContratacao.Repositories;
using TCC.ServiceCommerce.Infrastructure.Mensagens.Repositories;
using TCC.ServiceCommerce.Infrastructure.Perfis.Repositories;
using TCC.ServiceCommerce.Infrastructure.Posts.Repositories;
using TCC.ServiceCommerce.Infrastructure.Repositories.RepositoriesBase.Context;
using TCC.ServiceCommerce.Infrastructure.Usuarios.Repositories;

namespace TCC.ServiceCommerce.Initialize
{
    public static class DependencyResolver
    {
        public static void Resolve(ServiceContainer container)
        {
            #region Repositories
            container.Register(factory => new DataBaseContext(ConfigurationManager.ConnectionStrings["AppConnectionString"].ConnectionString),
                new PerScopeLifetime());
            container.Register<AppDataContext, AppDataContext>(new PerScopeLifetime());
            container.Register<IUserRepository, UsuarioRepository>(new PerScopeLifetime());
            container.Register<IPerfilRepository, PerfilRepository>(new PerScopeLifetime());
            container.Register<ICidadeRepository, CidadeRepository>(new PerScopeLifetime());
            container.Register<IPostRepository, PostRepository>(new PerScopeLifetime());
            container.Register<IHabilidadeRepository, HabilidadeRepository>(new PerScopeLifetime());
            container.Register<IMensagemRepository, MensagemRepository>(new PerScopeLifetime());
            container.Register<IDicionarioRepository, DicionarioRepository>(new PerScopeLifetime());
            container.Register<IHistoricoContratacaoRepository, HistoricoContratacaoRepository>(new PerScopeLifetime());
            #endregion

            #region Commands
            //Usuario
            container.Register<ICommandHandler<IncluirUsuarioCommand>, UsuarioCommandHandler>(new PerScopeLifetime());
            container.Register<ICommandHandler<ObterUsuarioCommand>, UsuarioCommandHandler>(new PerScopeLifetime());
            container.Register<ICommandHandler<ObterUsuarioEmailCommand>, UsuarioCommandHandler>(new PerScopeLifetime());
            container.Register<ICommandHandler<AlterarUsuarioCommand>, UsuarioCommandHandler>(new PerScopeLifetime());
            container.Register<ICommandHandler<AutenticarUsuarioCommand>, UsuarioCommandHandler>(new PerScopeLifetime());

            //Perfil
            container.Register<ICommandHandler<IncluirPerfilCommand>, PerfilCommandHandler>(new PerScopeLifetime());
            container.Register<ICommandHandler<AlterarPerfilCommand>, PerfilCommandHandler>(new PerScopeLifetime());
            container.Register<ICommandHandler<ObterPerfilUsuarioCommand>, PerfilCommandHandler>(new PerScopeLifetime());
            container.Register<ICommandHandler<ListarPerfisCommand>, PerfilCommandHandler>(new PerScopeLifetime());
            container.Register<ICommandHandler<IncluirPerfilHabilidadeCommand>, PerfilCommandHandler>(new PerScopeLifetime());
            container.Register<ICommandHandler<AlterarPerfilHabilidadeCommand>, PerfilCommandHandler>(new PerScopeLifetime());
            container.Register<ICommandHandler<ListarPerfilPorHabilidadeCommand>, PerfilCommandHandler>(new PerScopeLifetime());

            //Cidade
            container.Register<ICommandHandler<ObterCidadeCommand>, CidadeCommandHandler>(new PerScopeLifetime());
            container.Register<ICommandHandler<ListarCidadesCommand>, CidadeCommandHandler>(new PerScopeLifetime());

            //Habilidade
            container.Register<ICommandHandler<ListarHabilidadesCommand>, HabilidadeCommandHandler>(new PerScopeLifetime());
            container.Register<ICommandHandler<ObterHabilidadeCommand>, HabilidadeCommandHandler>(new PerScopeLifetime());

            //Post
            container.Register<ICommandHandler<IncluirPostCommand>, PostCommandHandler>(new PerScopeLifetime());
            container.Register<ICommandHandler<ExcluirPostCommand>, PostCommandHandler>(new PerScopeLifetime());
            container.Register<ICommandHandler<ListarPostsCommand>, PostCommandHandler>(new PerScopeLifetime());

            //Levenshtein
            container.Register<ICommandHandler<DicionarioCommand>, DicionarioCommandHandler>(new PerScopeLifetime());

            //Recomendacao
            container.Register<ICommandHandler<RecomendacaoCommand>, RecomendacaoCommandHandler>(new PerScopeLifetime());
            #endregion
        }
    }
}
