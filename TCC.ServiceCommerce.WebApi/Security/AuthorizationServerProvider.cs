﻿using Microsoft.Owin.Security.OAuth;
using System.Globalization;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using TCC.ServiceCommerce.Application.CommandHandlersBase;
using TCC.ServiceCommerce.Application.Usuarios.Commands.Inputs;
using TCC.ServiceCommerce.Application.Usuarios.Commands.Results;
using TCC.ServiceCommerce.Commom.DI;
using TCC.ServiceCommerce.Commom.Resources;

namespace TCC.ServiceCommerce.Security
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public AuthorizationServerProvider()
        { }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            await Task.FromResult(context.Validated());
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var command = new AutenticarUsuarioCommand();

            command.Email = context.UserName;
            command.Senha = context.Password;

            var commandHandler = DIContainer.Container.GetService<ICommandHandler<AutenticarUsuarioCommand>>();

            var result = (UsuarioCommandResult)commandHandler.Handle(command);

            if (!result.IsValid())
            {
                context.SetError("invalid_grant", Errors.InvalidCredentials);
                return;
            }


            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            identity.AddClaim(new Claim(ClaimTypes.Name, result.Usuario.Id.ToString(CultureInfo.InvariantCulture)));
            identity.AddClaim(new Claim(ClaimTypes.GivenName, result.Usuario.Email));
            //identity.AddClaim(new Claim("Usuario.Id", result.Usuario.Id.ToString(CultureInfo.InvariantCulture)));

            GenericPrincipal principal = new GenericPrincipal(identity, null);
            Thread.CurrentPrincipal = principal;

            context.Validated(identity);
            await Task.FromResult(0);
        }
    }
}
