﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.LevenshteinSearch.Commands;
using TCC.ServiceCommerce.Base;

namespace TCC.ServiceCommerce.Controllers
{
    [RoutePrefix("api")]
    public class SearchWordsController : ApiBaseController
    {
        public SearchWordsController()
        { }

        [HttpPut]
        [Route("palavras")]
        public async Task<HttpResponseMessage> ListarPalavras([FromBody]DicionarioCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<DicionarioCommand>().Handle(command));
        }
    }
}
