﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TCC.ServiceCommerce.Application.Cidades.Commands.Inputs;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Posts.Commands.Inputs;
using TCC.ServiceCommerce.Base;

namespace TCC.ServiceCommerce.Controllers
{
    [RoutePrefix("api")]
    public class PostController : ApiBaseController
    {
        public PostController()
        { }

        [HttpGet]
        [Route("usuarios/{usuarioId:min(1)}/posts")]
        public async Task<HttpResponseMessage> ListarPosts([FromUri]ListarPostsCommand command)
        {
            if (!command.IsValid())
            {
                return await BadRequest(command);
            }
            return await Response(GetCommandHandler<ListarPostsCommand>().Handle(command));
        }

        [HttpPost]
        [Authorize]
        [Route("posts")]
        public async Task<HttpResponseMessage> SalvarPost([FromBody]IncluirPostCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<IncluirPostCommand>().Handle(command));
        }

        [HttpDelete]
        [Authorize]
        [Route("posts/{id:min(1)}")]
        public async Task<HttpResponseMessage> ExcluirPost([FromUri]ExcluirPostCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<ExcluirPostCommand>().Handle(command));
        }
    }
}
