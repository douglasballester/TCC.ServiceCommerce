﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Recomendacoes.Commands;
using TCC.ServiceCommerce.Base;

namespace TCC.ServiceCommerce.Controllers
{
    [RoutePrefix("api")]
    public class RecomendacoesController : ApiBaseController
    {
        public RecomendacoesController()
        { }

        [HttpGet]
        [Route("recomendacoes")]
        public async Task<HttpResponseMessage> RecomendacaoServicos([FromUri]RecomendacaoCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<RecomendacaoCommand>().Handle(command));
        }
    }
}
