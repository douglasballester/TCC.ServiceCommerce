﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Usuarios.Commands.Inputs;
using TCC.ServiceCommerce.Base;

namespace TCC.ServiceCommerce.Controllers
{
    [RoutePrefix("api")]
    public class UsuarioController : ApiBaseController
    {
        public UsuarioController()
        { }

        [HttpPost]
        [Route("usuarios")]
        public async Task<HttpResponseMessage> IncluirUsuario([FromBody]IncluirUsuarioCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<IncluirUsuarioCommand>().Handle(command));
        }

        [HttpGet]
        [Route("usuarios/{id:min(1)}")]
        public async Task<HttpResponseMessage> ObterUsuario([FromUri]ObterUsuarioCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<ObterUsuarioCommand>().Handle(command));
        }

        [HttpGet]
        [Route("usuarios")]
        public async Task<HttpResponseMessage> ObterUsuarioPeloLogin([FromUri]ObterUsuarioCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<ObterUsuarioCommand>().Handle(command));
        }

        [HttpPut]
        [Authorize]
        [Route("usuarios/{id:min(1)}")]
        public async Task<HttpResponseMessage> AlterarUsuario([FromBody]AlterarUsuarioCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<AlterarUsuarioCommand>().Handle(command));
        }
    }
}
