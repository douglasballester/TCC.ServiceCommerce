﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Habilidades.Commands.Inputs;
using TCC.ServiceCommerce.Application.Perfis.Commands.Inputs;
using TCC.ServiceCommerce.Base;

namespace TCC.ServiceCommerce.Controllers
{
    [RoutePrefix("api")]
    public class HabilidadeController : ApiBaseController
    {
        public HabilidadeController()
        { }

        [HttpGet]
        [Route("habilidades/{id:min(1)}")]
        public async Task<HttpResponseMessage> ObterHabilidades([FromUri]ObterHabilidadeCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<ObterHabilidadeCommand>().Handle(command));
        }

        [HttpGet]
        [Route("habilidades")]
        public async Task<HttpResponseMessage> ObterHabilidades()
        {
            return await Response(GetCommandHandler<ListarHabilidadesCommand>().Handle(new ListarHabilidadesCommand()));
        }

        [HttpPost]
        [Authorize]
        [Route("perfis/{perfilId:min(1)}/habilidades")]
        public async Task<HttpResponseMessage> IncluirPerfilHabilidade([FromBody]IncluirPerfilHabilidadeCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<IncluirPerfilHabilidadeCommand>().Handle(command));
        }

        [HttpPut]
        [Authorize]
        [Route("perfis/{perfilId:min(1)}/habilidades")]
        public async Task<HttpResponseMessage> AlterarPerfilHabilidade([FromBody]AlterarPerfilHabilidadeCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<AlterarPerfilHabilidadeCommand>().Handle(command));
        }

        [HttpPut]
        [Authorize]
        [Route("perfis/{perfilId:min(1)}/habilidades{id:min(1)}")]
        public async Task<HttpResponseMessage> DeletarPerfilHabilidade([FromBody]DeletarPerfilHabilidadeCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<DeletarPerfilHabilidadeCommand>().Handle(command));
        }
    }
}
