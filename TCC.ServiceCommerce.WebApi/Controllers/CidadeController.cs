﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TCC.ServiceCommerce.Application.Cidades.Commands.Inputs;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Base;

namespace TCC.ServiceCommerce.Controllers
{
    [RoutePrefix("api")]
    public class CidadeController : ApiBaseController
    {
        public CidadeController()
        { }

        [HttpGet]
        [Route("cidades/{id:min(1)}")]
        public async Task<HttpResponseMessage> ObterCidade([FromUri]ObterCidadeCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<ObterCidadeCommand>().Handle(command));
        }

        [HttpGet]
        [Route("cidades")]
        public async Task<HttpResponseMessage> ListarCidades()
        {
            return await Response(GetCommandHandler<ListarCidadesCommand>().Handle(new ListarCidadesCommand()));
        }
    }
}
