﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Perfis.Commands.Inputs;
using TCC.ServiceCommerce.Base;

namespace TCC.ServiceCommerce.Controllers
{
    [RoutePrefix("api")]
    public class PerfilController : ApiBaseController
    {
        public PerfilController()
        { }

        [HttpPost]
        [Authorize]
        [Route("usuarios/{id:min(1)}/perfis")]
        public async Task<HttpResponseMessage> IncluirPerfil([FromBody]IncluirPerfilCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<IncluirPerfilCommand>().Handle(command));
        }

        [HttpPut]
        [Authorize]
        [Route("usuarios/{id:min(1)}/perfis/{perfilId:min(1)}")]
        public async Task<HttpResponseMessage> AlterarPerfil([FromBody]AlterarPerfilCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<AlterarPerfilCommand>().Handle(command));
        }

        [HttpGet]
        [Route("usuarios/{usuarioId:min(1)}/perfis")]
        public async Task<HttpResponseMessage> ObterPerfil([FromUri]ObterPerfilUsuarioCommand command)
        {
            if (!command.ValidateCommand())
            {
                return await BadRequest(command);
            }

            return await Response(GetCommandHandler<ObterPerfilUsuarioCommand>().Handle(command));
        }

        [HttpGet]
        [Route("perfis")]
        public async Task<HttpResponseMessage> ListarPerfis([FromUri]ListarPerfisCommand command)
        {
            return await Response(GetCommandHandler<ListarPerfisCommand>().Handle(new ListarPerfisCommand()));
        }

        [HttpGet]
        [Route("habilidades/{id:min(1)}/perfis")]
        public async Task<HttpResponseMessage> ListarPerfisPorHabilidade([FromUri]ListarPerfilPorHabilidadeCommand command)
        {
            return await Response(GetCommandHandler<ListarPerfilPorHabilidadeCommand>().Handle(command));
        }
    }
}
