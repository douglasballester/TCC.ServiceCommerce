﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TCC.ServiceCommerce.Application.CommandHandlersBase;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Commom.DI;
using TCC.ServiceCommerce.Commom.Extensions;
using TCC.ServiceCommerce.Commom.Notifications;

namespace TCC.ServiceCommerce.Base
{
    public abstract class ApiBaseController : ApiController
    {
        protected ICommandHandler<TCommand> GetCommandHandler<TCommand>() where TCommand : Command
        {
            return DIContainer.Container.GetService<ICommandHandler<TCommand>>();
        }

        protected Task<HttpResponseMessage> Response(CommandResult result)
        {
            bool isValid = result.IsValid();

            if (!isValid)
            {
                return BadRequest(result.Notifications);
            }

            return Task.FromResult(Request.CreateResponse(isValid ? HttpStatusCode.OK : HttpStatusCode.BadRequest, result));
        }

        protected Task<HttpResponseMessage> BadRequest(Command command)
        {
            return BadRequest(command?.Notifications);
        }

        protected Task<HttpResponseMessage> BadRequest(IEnumerable<Notification> notifications)
        {
            BadRequestCommandResult result = new BadRequestCommandResult();

            if (!notifications.IsNullOrEmpty())
            {
                result.AddNotifications(notifications);
            }

            return Task.FromResult(Request.CreateResponse(HttpStatusCode.BadRequest, result));
        }
    }
}