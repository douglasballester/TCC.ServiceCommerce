﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Dependencies;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Infrastructure.Repositories.RepositoriesBase.Context;

namespace TCC.ServiceCommerce.Base
{
    public class TransactionHandler : DelegatingHandler
    {
        private static readonly Type TypeOfDatabaseContext = typeof(DataBaseContext);

        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            IDependencyScope container = null;
            HttpRequestContext context = null;
            DataBaseContext database = null;
            HttpResponseMessage response = null;
            HttpStatusCode status = HttpStatusCode.OK;

            try
            {
                container = request.GetDependencyScope();
                context = request.GetRequestContext();
                database = container.GetService(TypeOfDatabaseContext) as DataBaseContext;

                //RuntimeContext.Current = new WebApiRuntimeContext(context.Principal);

                response = await base.SendAsync(request, cancellationToken);
                ICommandResult result;

                if (response.TryGetContentValue(out result))
                {
                    if (database != null)
                    {
                        if (database.ForcePersistence || result.IsValid())
                        {
                            database.Commit();
                        }
                        else
                        {
                            database.Rollback();
                        }
                    }
                }
                else if (response.StatusCode != HttpStatusCode.OK)
                {
                    status = response.StatusCode;
                }
            }
            //catch (BaseWebException bwe)
            //{
            //    status = bwe.Status;
            //}
            catch (Exception ex)
            {
                status = HttpStatusCode.BadRequest;
            }
            finally
            {
                if (status != HttpStatusCode.OK)
                {
                    response = request.CreateResponse(status);
                    database?.Rollback();
                }
            }

            return response;
        }
    }
}