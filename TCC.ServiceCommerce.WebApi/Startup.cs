﻿using LightInject;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Owin;
using System;
using System.Web.Http;
using TCC.ServiceCommerce.Base;
using TCC.ServiceCommerce.Commom.DI;
using TCC.ServiceCommerce.DI;
using TCC.ServiceCommerce.Initialize;
using TCC.ServiceCommerce.Security;

namespace TCC.ServiceCommerce
{
    public sealed class Startup
    {
        public DependencySetup DependencySetup { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            var container = new ServiceContainer();
            DependencyResolver.Resolve(container);
            DependencySetup = new DependencySetup(container);
            RegisterDependencies(config);
            
            ConfigureWebApi(config);
            ConfigureOAuth(app);

            config.MessageHandlers.Add(new TransactionHandler());
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        public void RegisterDependencies(HttpConfiguration config)
        {
            DependencySetup.Build();
            DependencySetup.Container.RegisterApiControllers();
            DependencySetup.Container.EnablePerWebRequestScope();
            DependencySetup.Container.EnableWebApi(config);
            DIContainer.Container = new DISetupContainer(DependencySetup.Container);
        }

        public static void ConfigureWebApi(HttpConfiguration config)
        {
            RegisterFormatters(config);
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/id",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        private static void RegisterFormatters(HttpConfiguration config)
        {
            var formatters = config.Formatters;
            formatters.Remove(formatters.XmlFormatter);

            var jsonSettings = formatters.JsonFormatter.SerializerSettings;
            jsonSettings.Formatting = Formatting.Indented;
            jsonSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            jsonSettings.NullValueHandling = NullValueHandling.Ignore;

            formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;
            JsonConvert.DefaultSettings = new Func<JsonSerializerSettings>(() => formatters.JsonFormatter.SerializerSettings);
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/api/security/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(2),
                Provider = new AuthorizationServerProvider()
            };


            FacebookAuthenticationOptions OAuthFacebookServerOptions = new FacebookAuthenticationOptions()
            {
                AppId = "1453518678084750",
                AppSecret = "e3dd37cc6fa42004948ca8a7d89572ee",
                Provider = new FacebookAuthProvider()
            };

            app.UseFacebookAuthentication(OAuthFacebookServerOptions);


            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}