﻿using LightInject;
using System;
using System.Collections.Generic;

namespace TCC.ServiceCommerce.DI
{
    public class DependencySetup
    {
        private readonly ServiceContainer container;
        private readonly List<Dependency> dependenciesToRegister;

        public DependencySetup(ServiceContainer container)
        {
            if (null == container)
            {
                throw new ArgumentNullException(nameof(container));
            }

            this.container = container;
            dependenciesToRegister = new List<Dependency>();
        }

        public event EventHandler<RegisterDependencyEventArgs> Registering;

        public ServiceContainer Container => container;

        public void Build()
        {
            foreach (Dependency dep in dependenciesToRegister)
            {
                RegisterDependencyEventArgs args = new RegisterDependencyEventArgs(dep.ServiceType, dep.Lifetime, container);
                OnRegistering(args);

                if (!args.IsOverrided)
                {
                    container.Register(dep.ServiceType, dep.ImplementationType, dep.Lifetime);
                }
            }

            dependenciesToRegister.Clear();
        }

        protected DependencySetup Register<TService, TImplementation>(ILifetime lifetime) where TImplementation : TService
        {
            dependenciesToRegister.Add(new Dependency(typeof(TService), typeof(TImplementation), lifetime));
            return this;
        }

        protected DependencySetup Register<TService>(Func<IServiceFactory, TService> factory, ILifetime lifetime)
        {
            container.Register(factory, lifetime);
            return this;
        }

        private void OnRegistering(RegisterDependencyEventArgs args)
        {
            Registering?.Invoke(this, args);
        }
    }

    public class RegisterDependencyEventArgs : EventArgs
    {
        public RegisterDependencyEventArgs(Type type, ILifetime lifetime, ServiceContainer container)
        {
            Type = type;
            Container = container;
            Lifetime = lifetime;
        }

        public Type Type { get; private set; }

        public ILifetime Lifetime { get; }

        public bool IsOverrided { get; set; }

        public ServiceContainer Container { get; }
    }

    internal struct Dependency
    {
        public Dependency(Type serviceType, Type implementationType, ILifetime lifetime)
        {
            ServiceType = serviceType;
            ImplementationType = implementationType;
            Lifetime = lifetime;
        }

        public Type ServiceType;

        public Type ImplementationType;

        public ILifetime Lifetime;
    }
}