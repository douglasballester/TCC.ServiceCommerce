﻿using LightInject;
using System;
using System.Collections.Generic;
using TCC.ServiceCommerce.Commom.DI;

namespace TCC.ServiceCommerce.DI
{
    public class DISetupContainer : IContainer
    {
        #region Fields
        public IServiceContainer Resolver { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Inicia uma nova instância da classe 
        /// </summary>
        /// <param name="container"></param>
        public DISetupContainer(IServiceContainer resolver)
        {
            Resolver = resolver;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Obtém um serviço do container.
        /// </summary>
        /// <param name="serviceType">O type do serviço</param>
        /// <returns>A instância do serviço.</returns>
        public object GetService(Type serviceType)
        {
            return Resolver.GetInstance(serviceType);
        }

        /// <summary>
        /// Obtém um serviço do container.
        /// </summary>
        /// <typeparam name="T">O type do serviço</typeparam>
        /// <returns>A instância do serviço.</returns>
        public T GetService<T>()
        {
            return (T)Resolver.GetInstance(typeof(T));
        }

        /// <summary>
        /// Obtém uma lista de serviços do container.
        /// </summary>
        /// <param name="serviceType">O type do serviço.</param>
        /// <returns>Uma lista com as instâncias do serviços.</returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return Resolver.GetAllInstances(serviceType);
        }

        /// <summary>
        /// Obtém uma lista de serviços do container.
        /// </summary>
        /// <typeparam name="T">O type do serviço.</typeparam>
        /// <returns>Uma lista com as instâncias do serviços.</returns>
        public IEnumerable<T> GetServices<T>()
        {
            return (IEnumerable<T>)Resolver.GetAllInstances(typeof(T));
        }

        /// <summary>
        /// Inicia um novo escopo no container.
        /// </summary>
        public void BeginScope()
        {
            Resolver.BeginScope();
        }

        /// <summary>
        /// Finaliza o escopo atual do container.
        /// </summary>
        public void EndCurrentScope()
        {
            var manager = Resolver.ScopeManagerProvider.GetScopeManager(Resolver);
            manager.EndScope(manager.CurrentScope);
        }
        #endregion
    }
}