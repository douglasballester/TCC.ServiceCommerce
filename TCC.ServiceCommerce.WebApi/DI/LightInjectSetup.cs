﻿using LightInject;
using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;

namespace TCC.ServiceCommerce.DI
{
    public class LightInjectSetup : IDependencyResolver
    {
        public IServiceContainer Resolver { get; set; }

        public LightInjectSetup(IServiceContainer resolver)
        {
            if (resolver == null)
            {
                throw new ArgumentNullException("container");
            }

            this.Resolver = resolver;
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return Resolver.GetInstance(serviceType);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return Resolver.GetAllInstances(serviceType);
            }
            catch (Exception ex)
            {
                return new List<object>();
            }
        }

        public IDependencyScope BeginScope()
        {
            return new LightInjectSetup(Resolver);
        }

        public void Dispose()
        {
            Resolver.Dispose();
        }
    }
}