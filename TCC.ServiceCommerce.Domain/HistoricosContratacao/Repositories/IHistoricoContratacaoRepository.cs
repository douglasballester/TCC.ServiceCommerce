﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Domain.HistoricosContratacao.Entities;

namespace TCC.ServiceCommerce.Domain.HistoricosContratacao.Repositories
{
    public interface IHistoricoContratacaoRepository
    {
        ICollection<HistoricoContratacao> ListarHistoricos();
    }
}
