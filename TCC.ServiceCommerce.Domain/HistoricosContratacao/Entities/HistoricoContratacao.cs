﻿using TCC.ServiceCommerce.Domain.Base.Entities;
using TCC.ServiceCommerce.Domain.Habilidades.Entities;
using TCC.ServiceCommerce.Domain.Usuarios.Entities;

namespace TCC.ServiceCommerce.Domain.HistoricosContratacao.Entities
{
    public class HistoricoContratacao : EntityBase
    {
        protected HistoricoContratacao()
        { }

        public HistoricoContratacao(long id) : base(id)
        { }

        public virtual long ContratanteId { get; private set; }

        public Usuario Contratante { get; private set; }

        public virtual long ContratadoId { get; private set; }

        public Usuario Contratado { get; private set; }

        public Habilidade ServicoContratado { get; private set; }
    }
}
