﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Domain.Cidades.Entities;

namespace TCC.ServiceCommerce.Domain.Cidades.Repositories
{
    public interface ICidadeRepository
    {
        Cidade Obter(long id);
        ICollection<Cidade> ObterTodas();
    }
}
