﻿using TCC.ServiceCommerce.Commom.Notifications;
using TCC.ServiceCommerce.Commom.Resources;

namespace TCC.ServiceCommerce.Domain.Cidades.Messages
{
    public static class CidadeMessages
    {
        public static readonly Notification CidadeNaoEncontrada = new Notification(Errors.CityNotFound);
    }
}