﻿using TCC.ServiceCommerce.Domain.Base.Entities;

namespace TCC.ServiceCommerce.Domain.Cidades.Entities
{
    public class Cidade : EntityBase
    {
        protected Cidade()
        { }

        public Cidade(long id, string descricao) : base(id)
        {
            Descricao = descricao;
        }

        public string Descricao { get; private set; }

        public void Alterar(Cidade cidadeAlterada)
        {
            Id = cidadeAlterada.Id;
            Descricao = cidadeAlterada.Descricao;
        }
    }
}
