﻿using TCC.ServiceCommerce.Domain.Mensagens.Entities;

namespace TCC.ServiceCommerce.Domain.Mensagens.Repositories
{
    public interface IMensagemRepository
    {
        Mensagem Salvar(Mensagem mensagem);
        void Delete(Mensagem mensagem);
        Mensagem Obter(long id);
    }
}
