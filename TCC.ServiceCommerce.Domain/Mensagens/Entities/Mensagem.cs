﻿using System;
using TCC.ServiceCommerce.Domain.Base.Entities;
using TCC.ServiceCommerce.Domain.Usuarios.Entities;

namespace TCC.ServiceCommerce.Domain.Mensagens.Entities
{
    public class Mensagem : EntityBase
    {
        protected Mensagem()
        { }

        public Mensagem(long id, string descricao, Usuario user) : base(id)
        {
            Descricao = descricao;
            UserSend = user;
            DataPostagem = DateTime.Now;
        }

        public string Descricao { get; private set; }

        public virtual long UserSendId { get; set; }

        public Usuario UserSend { get; private set; }

        public DateTime DataPostagem { get; private set; }
    }
}
