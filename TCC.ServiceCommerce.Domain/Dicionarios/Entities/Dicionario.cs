﻿using TCC.ServiceCommerce.Domain.Base.Entities;

namespace TCC.ServiceCommerce.Domain.Dicionarios.Entities
{
    public class Dicionario : EntityBase
    {
        public Dicionario()
        { }

        public Dicionario(long id) : base(id)
        {
        }

        public string Palavra { get; private set; }

        public string Fonetizar { get; private set; }
    }
}
