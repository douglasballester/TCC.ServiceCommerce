﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Domain.Dicionarios.Entities;

namespace TCC.ServiceCommerce.Domain.Dicionarios.Repositories
{
    public interface IDicionarioRepository
    {
        ICollection<Dicionario> ListarPalavras();
    }
}
