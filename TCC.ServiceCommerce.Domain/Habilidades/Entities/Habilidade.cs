﻿using TCC.ServiceCommerce.Domain.Base.Entities;

namespace TCC.ServiceCommerce.Domain.Habilidades.Entities
{
    public class Habilidade : EntityBase
    {
        protected Habilidade()
        { }

        public Habilidade(long id, string descricao) : base(id)
        {
            Descricao = descricao;
        }

        public string Descricao { get; private set; }

        public void Alterar(Habilidade habilidadeAlterada)
        {
            Id = habilidadeAlterada.Id;
            Descricao = habilidadeAlterada.Descricao;
        }
    }
}
