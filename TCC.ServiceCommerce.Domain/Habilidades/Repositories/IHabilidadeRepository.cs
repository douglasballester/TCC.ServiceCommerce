﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Domain.Habilidades.Entities;

namespace TCC.ServiceCommerce.Domain.Habilidades.Repositories
{
    public interface IHabilidadeRepository
    {
        Habilidade Obter(long id);
        ICollection<Habilidade> ObterTodas();
    }
}
