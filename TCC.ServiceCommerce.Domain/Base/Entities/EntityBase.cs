﻿using TCC.ServiceCommerce.Commom.Notifications;

namespace TCC.ServiceCommerce.Domain.Base.Entities
{
    public abstract class EntityBase : Notifiable
    {
        protected EntityBase()
        { }

        protected EntityBase(long id)
        {
            Id = id;
        }

        public virtual long Id { get; set; }
    }
}
