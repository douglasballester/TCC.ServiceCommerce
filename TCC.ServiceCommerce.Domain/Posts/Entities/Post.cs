﻿using TCC.ServiceCommerce.Domain.Base.Entities;
using TCC.ServiceCommerce.Domain.Mensagens.Entities;
using TCC.ServiceCommerce.Domain.Usuarios.Entities;

namespace TCC.ServiceCommerce.Domain.Posts.Entities
{
    public class Post : EntityBase
    {
        protected Post()
        { }

        public Post(long id) : base(id)
        { }

        public Post(Usuario user, Mensagem mensagem) : this(id: 0)
        {
            UserDestination = user;
            Mensagem = mensagem;
        }

        public virtual long UserDestinationId { get; set; }

        public Usuario UserDestination { get; private set; }

        public Mensagem Mensagem { get; private set; }

        public void Validar()
        {

        }
    }
}
