﻿using TCC.ServiceCommerce.Commom.Notifications;
using TCC.ServiceCommerce.Commom.Resources;

namespace TCC.ServiceCommerce.Domain.Posts.Messages
{
    public static class PostsMessages
    {
        public static readonly Notification RegistroNaoEncontrado = new Notification(Errors.RegisterNotFound);
    }
}