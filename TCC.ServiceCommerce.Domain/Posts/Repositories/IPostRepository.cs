﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Domain.Posts.Entities;

namespace TCC.ServiceCommerce.Domain.Posts.Repositories
{
    public interface IPostRepository
    {
        Post Salvar(Post post);
        ICollection<Post> ListarPostsUsuario(long usuarioId);
        Post Obter(long id);
        void Delete(Post post);
    }
}
