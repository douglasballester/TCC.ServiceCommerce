﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Domain.Usuarios.Entities;

namespace TCC.ServiceCommerce.Domain.Usuarios.Repositories
{
    public interface IUserRepository
    {
        Usuario Obter(long id);

        Usuario Obter(string email);

        Usuario Obter(long? id, string email);

        ICollection<Usuario> ListarTodos();

        Usuario Salvar(Usuario user);

        void Alterar(Usuario usuario);
    }
}
