﻿using TCC.ServiceCommerce.Domain.Base.Entities;

namespace TCC.ServiceCommerce.Domain.Usuarios.Entities
{
    public class InformacaoBancaria : EntityBase
    {
        protected InformacaoBancaria()
        { }

        public InformacaoBancaria(long id,
                                  string agencia,
                                  string conta,
                                  string dv) : base(id)
        {
            Agencia = agencia;
            Conta = conta;
            Dv = dv;
        }

        public virtual long UsuarioId { get; private set; }

        public virtual Usuario Usuario { get; private set; }

        public string Agencia { get; private set; }

        public string Conta { get; private set; }

        public string Dv { get; private set; }
    }
}
