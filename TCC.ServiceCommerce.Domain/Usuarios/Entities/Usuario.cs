﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Commom.Rules;
using TCC.ServiceCommerce.Domain.Base.Entities;
using TCC.ServiceCommerce.Domain.HistoricosContratacao.Entities;
using TCC.ServiceCommerce.Domain.Perfis.Entities;
using TCC.ServiceCommerce.Domain.Posts.Entities;
using TCC.ServiceCommerce.Domain.Usuarios.Rules;

namespace TCC.ServiceCommerce.Domain.Usuarios.Entities
{
    public class Usuario : EntityBase
    {
        public Usuario()
        { }

        public Usuario(string email,
                    string senha,
                    string nome,
                    bool ativo) : this(0,
                                       email,
                                       senha,
                                       nome,
                                       true)
        { }

        public Usuario(long id,
                    string email,
                    string senha,
                    string nome,
                    bool ativo) : base(id)
        {
            Email = email;
            Senha = senha;
            Nome = nome;
            Ativo = ativo;
        }

        public string Email { get; private set; }

        public string Senha { get; private set; }

        public string Nome { get; private set; }

        public Perfil Perfil { get; private set; }

        public InformacaoBancaria InformacaoBancaria { get; private set; }

        public ICollection<Post> Posts { get; private set; }

        public ICollection<HistoricoContratacao> HistoricoContratacao { get; private set; }

        public bool Ativo { get; private set; }

        public void Validar()
        {
            ValidationRules.For(this)
                .Add(new ValidarCamposObrigatoriosRule())
                .Add(new ValidarConfirmPasswordRule())
                .Add(new CriptografarSenhaRule())
                .Add(new ValidarUsuarioComEmailNaoExistente())
                .Run();
        }

        public void Alterar(Usuario usuario)
        {
            Nome = usuario.Nome;
            Email = usuario.Email;
            Senha = usuario.Senha;

            Validar();
        }

        internal void AtribuirSenha(string senha)
        {
            Senha = senha;
        }

        public void AtribuirPerfil(Perfil perfil)
        {
            Perfil = perfil;
        }
    }
}
