﻿using TCC.ServiceCommerce.Commom.Rules;
using TCC.ServiceCommerce.Commom.Validation;
using TCC.ServiceCommerce.Domain.Usuarios.Entities;

namespace TCC.ServiceCommerce.Domain.Usuarios.Rules
{
    public class CriptografarSenhaRule : RuleBase, IRule<Usuario>
    {
        public void Validate(Usuario target)
        {
            target.AtribuirSenha(PasswordAssertionConcern.Encrypt(target.Senha));
        }
    }
}
