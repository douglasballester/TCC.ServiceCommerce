﻿using TCC.ServiceCommerce.Commom.Rules;
using TCC.ServiceCommerce.Domain.Usuarios.Entities;
using TCC.ServiceCommerce.Domain.Usuarios.Messages;
using TCC.ServiceCommerce.Domain.Usuarios.Repositories;

namespace TCC.ServiceCommerce.Domain.Usuarios.Rules
{
    class ValidarUsuarioComEmailNaoExistente : RuleBase, IRule<Usuario>
    {
        public void Validate(Usuario target)
        {
            var usuarioMesmoEmail = GetRepository<IUserRepository>().Obter(target.Email);

            if (usuarioMesmoEmail != null && usuarioMesmoEmail.Id != target.Id)
                target.AddNotification(UsuarioMessages.UsuarioMesmoEmail);
        }
    }
}
