﻿using TCC.ServiceCommerce.Commom.Extensions;
using TCC.ServiceCommerce.Commom.Rules;
using TCC.ServiceCommerce.Domain.Usuarios.Entities;
using TCC.ServiceCommerce.Domain.Usuarios.Messages;

namespace TCC.ServiceCommerce.Domain.Usuarios.Rules
{
    public class ValidarCamposObrigatoriosRule : RuleBase, IRule<Usuario>
    {
        public void Validate(Usuario target)
        {
            if (target.Email.IsNullOrEmpty())
                target.AddNotification(UsuarioMessages.EmailNaoInformado);

            if (target.Senha.IsNullOrEmpty())
                target.AddNotification(UsuarioMessages.SenhaNaoInformada);

            if (target.Nome.IsNullOrEmpty())
                target.AddNotification(UsuarioMessages.NomeNaoInformado);
        }
    }
}
