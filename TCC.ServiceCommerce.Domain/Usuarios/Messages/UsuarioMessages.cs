﻿using TCC.ServiceCommerce.Commom.Notifications;
using TCC.ServiceCommerce.Commom.Resources;

namespace TCC.ServiceCommerce.Domain.Usuarios.Messages
{
    public static class UsuarioMessages
    {
        public static readonly Notification UsuarioNaoEncontrado = new Notification(Errors.UserNotFound);
        public static readonly Notification CredenciaisInvalidas = new Notification(Errors.InvalidCredentials);
        public static readonly Notification EmailNaoInformado = new Notification(Errors.EmptyEmail);
        public static readonly Notification SenhaNaoInformada = new Notification(Errors.EmptyPassword);
        public static readonly Notification NomeNaoInformado = new Notification(Errors.EmptyName);
        public static readonly Notification UsuarioMesmoEmail = new Notification(Errors.UserSameEmail);
        public static readonly Notification CampoExperienciaProfissionalObrigatorio = new Notification(Errors.CampoExperienciaProfissionalObrigatorio);
        public static readonly Notification CampoSobreObrigatorio = new Notification(Errors.CampoSobreObrigatorio);
        public static readonly Notification CampoCidadeObrigatorio = new Notification(Errors.CampoCidade);
        public static readonly Notification CampoNumeroDocumentoObrigatorio = new Notification(Errors.CampoNumeroDocumento);
        public static readonly Notification HabilidadesObrigatorio = new Notification(Errors.CampoHabilidades);
        public static readonly Notification PerfilNaoEcontrado = new Notification(Errors.PerfilNotFound);
        public static readonly Notification PerfilMesmoNumeroDocumento = new Notification(Errors.PerfilSameNumeroDocumento);
        public static readonly Notification ValorServicoMaiorQueZero = new Notification(Errors.ValorServico);
        public static readonly Notification PerfilObrigatorio = new Notification(Errors.PerfilObrigatorio);
        public static readonly Notification UsuarioPerfilUnico = new Notification(Errors.UsuarioPerfilUnico);
        public static readonly Notification RegistroNaoEncontrado = new Notification(Errors.RegisterNotFound);
        public static readonly Notification PerfisNaoEncontrados = new Notification(Errors.PerfisNaoEncontrados);
        public static readonly Notification PerfisNaoEncontradosParaHabilidade = new Notification(Errors.PerfisNaoEncontradosParaHabilidade);
    }
}
