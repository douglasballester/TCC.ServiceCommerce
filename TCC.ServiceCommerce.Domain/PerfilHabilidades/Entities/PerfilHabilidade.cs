﻿using TCC.ServiceCommerce.Commom.Rules;
using TCC.ServiceCommerce.Domain.Base.Entities;
using TCC.ServiceCommerce.Domain.Habilidades.Entities;
using TCC.ServiceCommerce.Domain.PerfilHabilidades.Rules;
using TCC.ServiceCommerce.Domain.Perfis.Entities;

namespace TCC.ServiceCommerce.Domain.PerfilHabilidades.Entities
{
    public class PerfilHabilidade : EntityBase
    {
        protected PerfilHabilidade()
        { }

        public PerfilHabilidade(decimal valorServico,
                                Habilidade habilidade) : this(id: 0,
                                                              valorServico: valorServico,
                                                              habilidade: habilidade)
        {

        }

        public PerfilHabilidade(long id,
                                decimal valorServico,
                                Habilidade habilidade) : base(id)
        {
            ValorServico = valorServico;
            Habilidade = habilidade;

            Validar();
        }

        public decimal ValorServico { get; private set; }

        public Perfil Perfil { get; private set; }

        public virtual long HabilidadeId { get; set; }

        public Habilidade Habilidade { get; private set; }

        private void Validar()
        {
            ValidationRules.For(this)
              .Add(new ValidarCamposObrigatoriosPerfilHabilidadeRule())
              .Run();
        }

        public void AtribuirPerfil(Perfil perfil)
        {
            Perfil = perfil;
        }

        public void AtribuirHabilidade(Habilidade habilidade)
        {
            Habilidade = habilidade;
        }

        public void Alterar(PerfilHabilidade perfilHabilidadeAlterado)
        {
            HabilidadeId = perfilHabilidadeAlterado.Habilidade.Id;
            //Habilidade.Alterar(perfilHabilidadeAlterado.Habilidade);
            ValorServico = perfilHabilidadeAlterado.ValorServico;
        }

        public override bool Equals(object obj)
        {
            var item = obj as PerfilHabilidade;

            if (item == null)
            {
                return false;
            }

            return Habilidade.Id.Equals(item.Habilidade.Id);
        }

        public override int GetHashCode()
        {
            return Habilidade.Id.GetHashCode();
        }
    }
}

