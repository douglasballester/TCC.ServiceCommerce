﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Domain.PerfilHabilidades.Entities;

namespace TCC.ServiceCommerce.Domain.PerfilHabilidades.Repositories
{
    public interface IPerfilHabilidadeRepository
    {
        ICollection<PerfilHabilidade> Obter(long perfilId);
        ICollection<PerfilHabilidade> Salvar(ICollection<PerfilHabilidade> perfilHabilidade);
        void Alterar(ICollection<PerfilHabilidade> perfilHabilidade);
    }
}
