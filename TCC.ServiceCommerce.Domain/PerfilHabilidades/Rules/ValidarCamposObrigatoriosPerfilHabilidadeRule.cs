﻿using TCC.ServiceCommerce.Commom.Rules;
using TCC.ServiceCommerce.Domain.PerfilHabilidades.Entities;
using TCC.ServiceCommerce.Domain.Usuarios.Messages;

namespace TCC.ServiceCommerce.Domain.PerfilHabilidades.Rules
{
    public class ValidarCamposObrigatoriosPerfilHabilidadeRule : RuleBase, IRule<PerfilHabilidade>
    {
        public void Validate(PerfilHabilidade target)
        {
            //if (target.ValorServico <= 0)
            //    target.AddNotification(UsuarioMessages.ValorServicoMaiorQueZero);

            if (target.Habilidade == null)
                target.AddNotification(UsuarioMessages.HabilidadesObrigatorio);
        }
    }
}
