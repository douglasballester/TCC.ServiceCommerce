﻿using System.Collections.Generic;
using System.Linq;
using TCC.ServiceCommerce.Commom.Extensions;
using TCC.ServiceCommerce.Commom.Rules;
using TCC.ServiceCommerce.Domain.Base.Entities;
using TCC.ServiceCommerce.Domain.Cidades.Entities;
using TCC.ServiceCommerce.Domain.PerfilHabilidades.Entities;
using TCC.ServiceCommerce.Domain.Perfis.Rules;
using TCC.ServiceCommerce.Domain.Usuarios.Entities;

namespace TCC.ServiceCommerce.Domain.Perfis.Entities
{
    public class Perfil : EntityBase
    {
        protected Perfil()
        { }

        public Perfil(string numeroDocumento,
                      Cidade cidade,
                      string experienciaProfissional,
                      string sobre,
                      ICollection<PerfilHabilidade> perfilHabilidade) : this(0,
                                           numeroDocumento,
                                           cidade,
                                           experienciaProfissional,
                                           sobre,
                                           perfilHabilidade)
        { }

        public Perfil(long id,
                       string numeroDocumento,
                       Cidade cidade,
                       string experienciaProfissional,
                       string sobre,
                       ICollection<PerfilHabilidade> perfilHabilidade) : base(id)
        {
            NumeroDocumento = numeroDocumento;
            Cidade = cidade;
            ExperienciaProfissinal = experienciaProfissional;
            Sobre = sobre;
            PerfilHabilidades = perfilHabilidade;
        }
        public virtual long UsuarioId { get; private set; }

        public virtual Usuario Usuario { get; private set; }

        public string NumeroDocumento { get; private set; }

        public virtual long CidadeId { get; set; }

        public Cidade Cidade { get; private set; }

        public string ExperienciaProfissinal { get; private set; }

        public string Sobre { get; private set; }

        public ICollection<PerfilHabilidade> PerfilHabilidades { get; private set; }

        public void Validar()
        {
            ValidationRules.For(this)
               .Add(new ValidarCamposObrigatoriosPerfilRule())
               .Add(new ValidarUsuarioMesmoNumeroDocumento())
               .Run();
        }

        public void Alterar(Perfil perfilAlterado)
        {
            perfilAlterado.Validar();

            if (!perfilAlterado.IsValid())
            {
                AddNotifications(perfilAlterado.Notifications);
                return;
            }

            NumeroDocumento = perfilAlterado.NumeroDocumento;
            ExperienciaProfissinal = perfilAlterado.ExperienciaProfissinal;
            Sobre = perfilAlterado.Sobre;

            if (CidadeId != perfilAlterado.Cidade.Id)
            {
                CidadeId = perfilAlterado.Cidade.Id;
                AtribuirCidade(perfilAlterado.Cidade);
            }
        }

        public void AtribuirUsuario(Usuario usuario)
        {
            UsuarioId = usuario.Id;
            Usuario = usuario;
        }

        public void AtribuirPerfilHabilidade(ICollection<PerfilHabilidade> perfilHabilidades)
        {
            PerfilHabilidades = perfilHabilidades;
        }

        public void AtribuirCidade(Cidade cidade)
        {
            Cidade = cidade;
        }
    }
}
