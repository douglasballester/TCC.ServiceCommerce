﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Domain.PerfilHabilidades.Entities;
using TCC.ServiceCommerce.Domain.Perfis.Entities;

namespace TCC.ServiceCommerce.Domain.Perfis.Repositories
{
    public interface IPerfilRepository
    {
        Perfil Obter(long id);
        Perfil Obter(string numeroDocumento);
        Perfil ObterPerfilUsuario(long usuarioId);
        ICollection<Perfil> ListarPerfis();
        Perfil Salvar(Perfil perfil);
        Perfil Alterar(Perfil perfil);
        Perfil SalvarPerfilHabilidades(Perfil perfil);
        ICollection<PerfilHabilidade> SalvarPerfilHabilidades(ICollection<PerfilHabilidade> perfilHabilidade);
        ICollection<PerfilHabilidade> DeletarPerfilHabilidades(ICollection<PerfilHabilidade> perfilHabilidade);
    }
}