﻿using TCC.ServiceCommerce.Commom.Rules;
using TCC.ServiceCommerce.Domain.Perfis.Entities;
using TCC.ServiceCommerce.Domain.Perfis.Repositories;
using TCC.ServiceCommerce.Domain.Usuarios.Messages;

namespace TCC.ServiceCommerce.Domain.Perfis.Rules
{
    class ValidarUsuarioMesmoNumeroDocumento : RuleBase, IRule<Perfil>
    {
        public void Validate(Perfil target)
        {
            var perfilMesmoNumeroDocumento = GetRepository<IPerfilRepository>().Obter(target.NumeroDocumento);

            if (perfilMesmoNumeroDocumento != null && perfilMesmoNumeroDocumento.Id != target.Id)
                target.AddNotification(UsuarioMessages.PerfilMesmoNumeroDocumento);
        }
    }
}
