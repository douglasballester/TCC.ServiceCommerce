﻿using TCC.ServiceCommerce.Commom.Extensions;
using TCC.ServiceCommerce.Commom.Rules;
using TCC.ServiceCommerce.Domain.Perfis.Entities;
using TCC.ServiceCommerce.Domain.Usuarios.Messages;

namespace TCC.ServiceCommerce.Domain.Perfis.Rules
{
    public class ValidarCamposObrigatoriosPerfilRule : RuleBase, IRule<Perfil>
    {
        public void Validate(Perfil target)
        {
            if (target.ExperienciaProfissinal.IsNullOrEmpty())
                target.AddNotification(UsuarioMessages.CampoExperienciaProfissionalObrigatorio);

            if (target.Sobre.IsNullOrEmpty())
                target.AddNotification(UsuarioMessages.CampoSobreObrigatorio);

            if (target.Cidade == null)
                target.AddNotification(UsuarioMessages.CampoCidadeObrigatorio);

            if (target.NumeroDocumento.IsNullOrEmpty())
                target.AddNotification(UsuarioMessages.CampoNumeroDocumentoObrigatorio);
        }
    }
}
