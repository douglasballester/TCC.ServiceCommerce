﻿using System;
using TCC.ServiceCommerce.Application.Usuarios.Dtos;
using TCC.ServiceCommerce.Domain.Mensagens.Entities;
using TCC.ServiceCommerce.Domain.Usuarios.Entities;

namespace TCC.ServiceCommerce.Application.Mensagens.Dtos
{
    public class MensagemDto
    {
        public long Id { get; set; }

        public string Descricao { get; set; }

        public UsuarioDto UserSend { get; set; }

        public DateTime DataPostagem { get; set; }

        public static MensagemDto Mapper(Mensagem mensagem)
        {
            if (mensagem == null)
                return null;

            return new MensagemDto
            {
                Id = mensagem.Id,
                Descricao = mensagem.Descricao,
                UserSend = UsuarioDto.Mapper(mensagem.UserSend),
                DataPostagem = mensagem.DataPostagem
            };
        }

        public Mensagem MapperIncluirMensagem()
        {
            return new Mensagem(id: Id,
                                descricao: Descricao,
                                user: new Usuario(UserSend.Id, null, null, null, true));
        }

        //public Usuario MapperAlterarUsuario()
        //{
        //    return new Usuario(id: Id,
        //                       email: Email,
        //                       senha: Senha,
        //                       nome: Nome,
        //                       ativo: true);
        //}
    }
}