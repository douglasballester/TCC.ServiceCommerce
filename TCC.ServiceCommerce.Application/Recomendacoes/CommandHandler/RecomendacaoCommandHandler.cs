﻿using System.Linq;
using TCC.ServiceCommerce.Application.CommandHandlersBase;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Habilidades.Commands.Results;
using TCC.ServiceCommerce.Application.Habilidades.Dtos;
using TCC.ServiceCommerce.Application.Recomendacoes.Commands;
using TCC.ServiceCommerce.Domain.HistoricosContratacao.Repositories;

namespace TCC.ServiceCommerce.Application.Recomendacoes.CommandHandlers
{
    public class RecomendacaoCommandHandler : CommandHandlerBase,
                                      ICommandHandler<RecomendacaoCommand>
    {
        public CommandResult Handle(RecomendacaoCommand command)
        {
            var result = new HabilidadesCommandResult();
            var historicosContratacao = GetService<IHistoricoContratacaoRepository>().ListarHistoricos();
            var historicosContratacaoFilter = historicosContratacao.Where(hc => hc.ServicoContratado.Id == command.HabilidadeId).ToList();

           var sugestaoHabilidades = historicosContratacaoFilter.SelectMany(hc => hc.Contratante.HistoricoContratacao
                                                                             .Where(chc => chc.ServicoContratado.Id != command.HabilidadeId)
                                                                             .Select(x => x.ServicoContratado)
                                                                             .OrderBy(x => x.Descricao)
                                                                             .ToList());


            var sugestaOrderByOcorrencia = sugestaoHabilidades.GroupBy(h => h.Id).OrderByDescending(h => h.Count()).SelectMany(h => h).Distinct().ToList();
       

            result.Habilidades = sugestaOrderByOcorrencia.Select(sh => HabilidadeDto.Mapper(sh)).ToList();

            return result; 
        }
    }
}