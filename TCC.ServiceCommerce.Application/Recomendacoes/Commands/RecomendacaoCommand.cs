﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Recomendacoes.Commands
{
    public class RecomendacaoCommand : Command
    {
        public long HabilidadeId { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
