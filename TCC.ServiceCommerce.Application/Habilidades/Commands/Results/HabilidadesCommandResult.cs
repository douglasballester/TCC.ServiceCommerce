﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Habilidades.Dtos;

namespace TCC.ServiceCommerce.Application.Habilidades.Commands.Results
{
    public class HabilidadesCommandResult : CommandResult
    {
        public ICollection<HabilidadeDto> Habilidades { get; set; }
    }
}
