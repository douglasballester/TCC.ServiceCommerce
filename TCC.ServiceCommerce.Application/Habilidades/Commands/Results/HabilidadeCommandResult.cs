﻿using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Habilidades.Dtos;

namespace TCC.ServiceCommerce.Application.Habilidades.Commands.Results
{
    public class HabilidadeCommandResult : CommandResult
    {
        public HabilidadeDto Habilidade { get; set; }
    }
}
