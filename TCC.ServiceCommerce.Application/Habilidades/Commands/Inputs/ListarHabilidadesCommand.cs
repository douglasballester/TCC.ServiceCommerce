﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Habilidades.Commands.Inputs
{
    public class ListarHabilidadesCommand : Command
    {
        public override bool Validate()
        {
            return IsValid();
        }
    }
}
