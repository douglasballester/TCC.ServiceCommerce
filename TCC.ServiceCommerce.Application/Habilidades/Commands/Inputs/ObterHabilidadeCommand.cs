﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Habilidades.Commands.Inputs
{
    public class ObterHabilidadeCommand : Command
    {
        public long Id { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
