﻿using TCC.ServiceCommerce.Domain.Habilidades.Entities;

namespace TCC.ServiceCommerce.Application.Habilidades.Dtos
{
    public class HabilidadeDto
    {
        public long Id { get; set; }

        public string Descricao { get; set; }

        public static HabilidadeDto Mapper(Habilidade habilidade)
        {
            if (habilidade == null)
                return null;

            return new HabilidadeDto
            {
                Id = habilidade.Id,
                Descricao = habilidade.Descricao
            };
        }

        public Habilidade MapperIncluirHabilidade()
        {
            return new Habilidade(Id, Descricao);
        }
    }
}
