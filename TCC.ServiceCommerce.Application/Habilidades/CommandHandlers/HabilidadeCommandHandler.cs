﻿using System.Linq;
using TCC.ServiceCommerce.Application.CommandHandlersBase;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Habilidades.Commands.Inputs;
using TCC.ServiceCommerce.Application.Habilidades.Commands.Results;
using TCC.ServiceCommerce.Application.Habilidades.Dtos;
using TCC.ServiceCommerce.Commom.Extensions;
using TCC.ServiceCommerce.Domain.Cidades.Messages;
using TCC.ServiceCommerce.Domain.Habilidades.Repositories;

namespace TCC.ServiceCommerce.Application.Habilidades.CommandHandlers
{
    public class HabilidadeCommandHandler : CommandHandlerBase,
                                        ICommandHandler<ObterHabilidadeCommand>,
                                        ICommandHandler<ListarHabilidadesCommand>
    {
        public CommandResult Handle(ListarHabilidadesCommand command)
        {
            var result = new HabilidadesCommandResult();

            var habilidades = GetService<IHabilidadeRepository>().ObterTodas();

            if (habilidades.IsNullOrEmpty())
            {
                result.AddNotification(CidadeMessages.CidadeNaoEncontrada);
                return result;
            }

            result.Habilidades = habilidades.Select(c => HabilidadeDto.Mapper(c)).ToList();

            return result;
        }

        public CommandResult Handle(ObterHabilidadeCommand command)
        {
            var result = new HabilidadeCommandResult();

            var habilidade = GetService<IHabilidadeRepository>().Obter(command.Id);

            if (habilidade == null)
            {
                result.AddNotification(CidadeMessages.CidadeNaoEncontrada);
                return result;
            }

            result.Habilidade = HabilidadeDto.Mapper(habilidade);

            return result;
        }
    }
}