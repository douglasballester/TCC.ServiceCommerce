﻿using System.Collections.Generic;

namespace TCC.ServiceCommerce.Application.LevenshteinSearch.Dtos
{
    public class LevenshteinDto
    {
        public ICollection<string> Palavras { get; set; }

    }
}
