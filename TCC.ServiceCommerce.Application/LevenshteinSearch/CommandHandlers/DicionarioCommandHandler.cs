﻿using System.Linq;
using TCC.ServiceCommerce.Application.CommandHandlersBase;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.LevenshteinSearch.Commands;
using TCC.ServiceCommerce.Commom.EditDistance;
using TCC.ServiceCommerce.Domain.Dicionarios.Repositories;

namespace TCC.ServiceCommerce.Application.LevenshteinSearch.CommandHandlers
{
    public class DicionarioCommandHandler : CommandHandlerBase,
                                             ICommandHandler<DicionarioCommand>
    {
        public CommandResult Handle(DicionarioCommand command)
        {
            var result = new DicionarioCommandResult();
            var dicionario = GetService<IDicionarioRepository>().ListarPalavras();

            var palavras = dicionario.Select(d => d.Palavra).Where(d => EditDistanceLevenshtein.EditDistance(d, command.Palavra.ToUpper()) <= 2).ToList();

            result.Palavras = palavras;

            return result;
        }
    }
}
