﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.LevenshteinSearch.Commands
{
    public class DicionarioCommand : Command
    {
        public string Palavra { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
