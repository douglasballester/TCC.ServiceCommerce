﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.LevenshteinSearch.Dtos;

namespace TCC.ServiceCommerce.Application.LevenshteinSearch.Commands
{
    public class DicionarioCommandResult : CommandResult
    {
        //public LevenshteinDto LevenshteinDto { get; set; }
        public ICollection<string> Palavras { get; set; }
    }
}
