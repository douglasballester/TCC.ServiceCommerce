﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.CommandHandlersBase
{
    public interface ICommandHandler<in TCommand> where TCommand : Command
    {
        CommandResult Handle(TCommand command);
    }
}
