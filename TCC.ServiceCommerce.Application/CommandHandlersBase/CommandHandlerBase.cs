﻿using TCC.ServiceCommerce.Commom.DI;

namespace TCC.ServiceCommerce.Application.CommandHandlersBase
{
    public class CommandHandlerBase
    {
        protected CommandHandlerBase()
        {
        }

        protected TService GetService<TService>()
        {
            return DIContainer.Container.GetService<TService>();
        }
    }
}
