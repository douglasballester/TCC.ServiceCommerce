﻿using TCC.ServiceCommerce.Domain.Cidades.Entities;

namespace TCC.ServiceCommerce.Application.Cidades.Dtos
{
    public class CidadeDto
    {
        public long Id { get; set; }

        public string Descricao { get; set; }


        public static CidadeDto Mapper(Cidade cidade)
        {
            if (cidade == null)
                return null;

            return new CidadeDto
            {
                Id = cidade.Id,
                Descricao = cidade.Descricao
            };
        }

        public Cidade MapperIncluirCidade()
        {
            return new Cidade(id: Id, descricao: Descricao);
        }

        public Cidade MapperAlterarCidade()
        {
            return new Cidade(id: Id, descricao: Descricao);
        }
    }
}
