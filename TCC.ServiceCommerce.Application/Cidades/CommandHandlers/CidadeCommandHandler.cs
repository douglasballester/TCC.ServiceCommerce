﻿using System;
using System.Linq;
using TCC.ServiceCommerce.Application.Cidades.Commands.Inputs;
using TCC.ServiceCommerce.Application.Cidades.Commands.Results;
using TCC.ServiceCommerce.Application.Cidades.Dtos;
using TCC.ServiceCommerce.Application.CommandHandlersBase;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Commom.Extensions;
using TCC.ServiceCommerce.Domain.Cidades.Messages;
using TCC.ServiceCommerce.Domain.Cidades.Repositories;

namespace TCC.ServiceCommerce.Application.Cidades.CommandHandlers
{
    public class CidadeCommandHandler : CommandHandlerBase,
                                        ICommandHandler<ObterCidadeCommand>,
                                        ICommandHandler<ListarCidadesCommand>
    {
        public CommandResult Handle(ListarCidadesCommand command)
        {
            var result = new CidadesCommandResult();

            var cidades = GetService<ICidadeRepository>().ObterTodas();

            if (cidades.IsNullOrEmpty())
            {
                result.AddNotification(CidadeMessages.CidadeNaoEncontrada);
                return result;
            }

            result.Cidades = cidades.Select(c => CidadeDto.Mapper(c)).ToList();

            return result;
        }

        public CommandResult Handle(ObterCidadeCommand command)
        {
            var result = new CidadeCommandResult();

            var cidade = GetService<ICidadeRepository>().Obter(command.Id);

            if (cidade == null)
            {
                result.AddNotification(CidadeMessages.CidadeNaoEncontrada);
                return result;
            }

            result.Cidade = CidadeDto.Mapper(cidade);

            return result;
        }
    }
}