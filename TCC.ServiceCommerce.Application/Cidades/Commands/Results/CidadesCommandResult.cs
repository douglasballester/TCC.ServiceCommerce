﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Application.Cidades.Dtos;
using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Cidades.Commands.Results
{
    public class CidadesCommandResult : CommandResult
    {
        public ICollection<CidadeDto> Cidades { get; set; }
    }
}
