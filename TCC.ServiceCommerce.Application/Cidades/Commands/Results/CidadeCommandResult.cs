﻿using TCC.ServiceCommerce.Application.Cidades.Dtos;
using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Cidades.Commands.Results
{
    public class CidadeCommandResult : CommandResult
    {
        public CidadeDto Cidade { get; set; }
    }
}
