﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Cidades.Commands.Inputs
{
    public class ObterCidadeCommand : Command
    {
        public long Id { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
