﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Cidades.Commands.Inputs
{
    public class ListarCidadesCommand : Command
    {
        public override bool Validate()
        {
            return IsValid();
        }
    }
}
