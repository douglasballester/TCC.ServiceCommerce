﻿namespace TCC.ServiceCommerce.Application.CommandsBase
{
    public interface ICommandResult
    {
        bool IsValid();
    }
}
