﻿namespace TCC.ServiceCommerce.Application.CommandsBase
{
    public static class CommandExtensions
    {
        public static bool ValidateCommand(this Command command)
        {
            return command != null && command.Validate();
        }
    }
}
