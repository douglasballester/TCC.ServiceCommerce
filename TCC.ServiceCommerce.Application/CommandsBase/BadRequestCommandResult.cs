﻿namespace TCC.ServiceCommerce.Application.CommandsBase
{
    public sealed class BadRequestCommandResult : CommandResult
    {
        public override bool IsValid()
        {
            return false;
        }
    }
}
