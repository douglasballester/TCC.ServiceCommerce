﻿using TCC.ServiceCommerce.Commom.Notifications;

namespace TCC.ServiceCommerce.Application.CommandsBase
{
    public abstract class Command : Notifiable
    {
        public abstract bool Validate();
    }
}
