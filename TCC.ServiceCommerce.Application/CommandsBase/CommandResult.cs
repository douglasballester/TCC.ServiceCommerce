﻿using TCC.ServiceCommerce.Commom.Notifications;

namespace TCC.ServiceCommerce.Application.CommandsBase
{
    public abstract class CommandResult : Notifiable, ICommandResult
    {
        public static CommandResult DefaultCommandResult
        {
            get
            {
                return new DefaultCommandResult();
            }
        }
    }
}
