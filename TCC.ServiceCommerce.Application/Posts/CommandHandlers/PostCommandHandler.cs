﻿using System.Linq;
using TCC.ServiceCommerce.Application.CommandHandlersBase;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Posts.Commands.Inputs;
using TCC.ServiceCommerce.Application.Posts.Commands.Results;
using TCC.ServiceCommerce.Application.Posts.Dtos;
using TCC.ServiceCommerce.Commom.Extensions;
using TCC.ServiceCommerce.Domain.Mensagens.Repositories;
using TCC.ServiceCommerce.Domain.Posts.Repositories;

namespace TCC.ServiceCommerce.Application.Posts.CommandHandlers
{
    public class PostCommandHandler : CommandHandlerBase,
                                      ICommandHandler<IncluirPostCommand>,
                                      ICommandHandler<ExcluirPostCommand>,
                                      ICommandHandler<ListarPostsCommand>
    {
        public CommandResult Handle(ListarPostsCommand command)
        {
            var result = new ListarPostsCommandResult();

            var posts = GetService<IPostRepository>().ListarPostsUsuario(command.UsuarioId);

            if (posts.IsNullOrEmpty())
            {
                //result.AddNotification(PostsMessages.RegistroNaoEncontrado);
                return result;
            }

            result.Posts = posts.Select(p => PostDto.Mapper(p)).ToList();

            return result;
        }

        public CommandResult Handle(IncluirPostCommand command)
        {
            var result = new PostCommandResult();

            var post = command.Post.MapperIncluirPost();

            post.Validar();

            if (!post.IsValid())
            {
                result.AddNotifications(post.Notifications);
                return result;
            }

            post = GetService<IPostRepository>().Salvar(post);
            result.Post = PostDto.Mapper(post);

            return result;
        }

        public CommandResult Handle(ExcluirPostCommand command)
        {
            var result = new PostCommandResult();

            var post = GetService<IPostRepository>().Obter(command.Id);

            if(post != null)
            {
                GetService<IMensagemRepository>().Delete(post.Mensagem);
                GetService<IPostRepository>().Delete(post);
            }
            
            return result;
        }
    }
}