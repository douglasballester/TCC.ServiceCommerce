﻿using TCC.ServiceCommerce.Application.Mensagens.Dtos;
using TCC.ServiceCommerce.Application.Usuarios.Dtos;
using TCC.ServiceCommerce.Domain.Mensagens.Entities;
using TCC.ServiceCommerce.Domain.Posts.Entities;
using TCC.ServiceCommerce.Domain.Usuarios.Entities;

namespace TCC.ServiceCommerce.Application.Posts.Dtos
{
    public class PostDto
    {
        public long Id { get; set; }

        public UsuarioDto UserDestination { get; set; }

        public MensagemDto Mensagem { get; set; }

        public static PostDto Mapper(Post post)
        {
            if (post == null)
                return null;

            return new PostDto
            {
                Id = post.Id,
                UserDestination = UsuarioDto.Mapper(post.UserDestination),
                Mensagem = MensagemDto.Mapper(post.Mensagem)
            };
        }

        public Post MapperIncluirPost()
        {
            return new Post(new Usuario(UserDestination.Id, null, null, null, true),
                            new Mensagem(Mensagem.Id, Mensagem.Descricao, 
                            new Usuario(Mensagem.UserSend.Id, null, null, null, true)));
        }

        //public Usuario MapperAlterarUsuario()
        //{
        //    return new Usuario(id: Id,
        //                       email: Email,
        //                       senha: Senha,
        //                       nome: Nome,
        //                       ativo: true);
        //}
    }
}
