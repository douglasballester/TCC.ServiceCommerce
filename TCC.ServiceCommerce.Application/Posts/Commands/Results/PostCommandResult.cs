﻿using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Posts.Dtos;

namespace TCC.ServiceCommerce.Application.Posts.Commands.Results
{
    public class PostCommandResult : CommandResult
    {
        public PostDto Post { get; set; }
    }
}
