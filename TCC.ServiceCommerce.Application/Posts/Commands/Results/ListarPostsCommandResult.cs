﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Posts.Dtos;

namespace TCC.ServiceCommerce.Application.Posts.Commands.Results
{
    public class ListarPostsCommandResult : CommandResult
    {
        public ICollection<PostDto> Posts { get; set; }
    }
}
