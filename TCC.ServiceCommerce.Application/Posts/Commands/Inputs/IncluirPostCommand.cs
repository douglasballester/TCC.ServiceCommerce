﻿using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Posts.Dtos;
using TCC.ServiceCommerce.Application.Usuarios.Dtos;

namespace TCC.ServiceCommerce.Application.Posts.Commands.Inputs
{
    public class IncluirPostCommand : Command
    {
        public UsuarioDto Usuario { get; set; }

        public PostDto Post { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
