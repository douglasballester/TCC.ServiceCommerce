﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Posts.Commands.Inputs
{
    public class ExcluirPostCommand : Command
    {
        public long Id { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
