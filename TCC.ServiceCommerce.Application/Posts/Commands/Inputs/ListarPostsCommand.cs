﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Posts.Commands.Inputs
{
    public class ListarPostsCommand : Command
    {
        public long UsuarioId { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
