﻿using System;
using System.Collections.Generic;
using System.Linq;
using TCC.ServiceCommerce.Application.CommandHandlersBase;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Perfis.Commands.Inputs;
using TCC.ServiceCommerce.Application.Perfis.Commands.Results;
using TCC.ServiceCommerce.Application.Perfis.Dtos;
using TCC.ServiceCommerce.Commom.Extensions;
using TCC.ServiceCommerce.Domain.Habilidades.Repositories;
using TCC.ServiceCommerce.Domain.PerfilHabilidades.Entities;
using TCC.ServiceCommerce.Domain.Perfis.Entities;
using TCC.ServiceCommerce.Domain.Perfis.Repositories;
using TCC.ServiceCommerce.Domain.Usuarios.Messages;
using TCC.ServiceCommerce.Domain.Usuarios.Repositories;

namespace TCC.ServiceCommerce.Application.Perfis.CommandHandlers
{
    public class PerfilCommandHandler : CommandHandlerBase,
                                        ICommandHandler<IncluirPerfilCommand>,
                                        ICommandHandler<AlterarPerfilCommand>,
                                        ICommandHandler<IncluirPerfilHabilidadeCommand>,
                                        ICommandHandler<AlterarPerfilHabilidadeCommand>,
                                        ICommandHandler<ObterPerfilUsuarioCommand>,
                                        ICommandHandler<ListarPerfisCommand>,
                                        ICommandHandler<ListarPerfilPorHabilidadeCommand>
    {
        public CommandResult Handle(IncluirPerfilHabilidadeCommand command)
        {
            var result = new PerfilHabilidadeCommandResult();

            var perfil = GetService<IPerfilRepository>().Obter(command.PerfilId);

            if (perfil == null)
            {
                result.AddNotification(UsuarioMessages.PerfilNaoEcontrado);
                return result;
            }

            var perfilHabilidades = command.PerfilHabilidades.Select(ph => ph.MapperIncluirPerfilHabilidade()).ToList();
            perfil.AtribuirPerfilHabilidade(perfilHabilidades);
            perfil = GetService<IPerfilRepository>().SalvarPerfilHabilidades(perfil);

            result.PerfilHabilidade = perfil.PerfilHabilidades.Select(ph => PerfilHabilidadeDto.Mapper(ph)).ToList();

            return result;
        }

        public CommandResult Handle(ObterPerfilUsuarioCommand command)
        {
            var result = new PerfilCommandResult();

            var perfil = GetService<IPerfilRepository>().ObterPerfilUsuario(command.UsuarioId);

            if (perfil == null)
            {
                //result.AddNotification(UsuarioMessages.PerfilNaoEcontrado);
                return result;
            }

            foreach (var ph in perfil.PerfilHabilidades)
            {
                ph.AtribuirHabilidade(GetService<IHabilidadeRepository>().Obter(ph.HabilidadeId));
            }

            result.Perfil = PerfilDto.Mapper(perfil);

            return result;
        }

        public CommandResult Handle(ListarPerfilPorHabilidadeCommand command)
        {
            var result = new ListarPerfilCommandResult();

            var perfis = GetService<IPerfilRepository>().ListarPerfis();

            if (perfis.IsNullOrEmpty())
            {
                result.AddNotification(UsuarioMessages.RegistroNaoEncontrado);
                return result;
            }


            var perfisRetorno = new List<Perfil>();
            foreach (var perfil in perfis)
            {
                var perf = perfil.PerfilHabilidades.Where(ph => ph.Habilidade.Id == command.Id).FirstOrDefault();

                if(perf != null)
                {
                    perfisRetorno.Add(perf.Perfil);
                }
            }

            if (perfisRetorno.IsNullOrEmpty())
            {
                result.AddNotification(UsuarioMessages.PerfisNaoEncontradosParaHabilidade);
                return result;
            }

            result.Perfis = perfisRetorno.Select(p => PerfilDto.Mapper(p)).ToList();

            return result;
        }

        public CommandResult Handle(ListarPerfisCommand command)
        {
            var result = new ListarPerfilCommandResult();

            var perfis = GetService<IPerfilRepository>().ListarPerfis();

            if (perfis.IsNullOrEmpty())
            {
                result.AddNotification(UsuarioMessages.PerfisNaoEncontrados);
                return result;
            }

            //Ver necessidade de retornar apenas usuarios com habiliades cadastradas;

            result.Perfis = perfis.Select(p => PerfilDto.Mapper(p)).ToList();

            return result;
        }

        public CommandResult Handle(AlterarPerfilHabilidadeCommand command)
        {
            var result = new PerfilHabilidadeCommandResult();
            var repository = GetService<IPerfilRepository>();
            var perfil = repository.Obter(command.PerfilId);

            if (perfil == null)
            {
                result.AddNotification(UsuarioMessages.PerfilNaoEcontrado);
                return result;
            }

            if (perfil.PerfilHabilidades.IsNullOrEmpty())
            {
                var perfilHabilidadesNew = command.PerfilHabilidades.Select(ph => ph.MapperIncluirPerfilHabilidade()).ToList();
                perfil.AtribuirPerfilHabilidade(perfilHabilidadesNew);
                repository.SalvarPerfilHabilidades(perfil);
            }
            else
            {
                ICollection<PerfilHabilidade> habilidadesToSave = new List<PerfilHabilidade>();
                ICollection<PerfilHabilidade> habilidadesToDelete = new List<PerfilHabilidade>();
                var perfilHabilidadesNew = command.PerfilHabilidades.Select(ph => ph.MapperIncluirPerfilHabilidade()).ToList();

                var habildadesNovas = perfilHabilidadesNew.Except(perfil.PerfilHabilidades);

                foreach (var item in habildadesNovas)
                {
                    item.AtribuirPerfil(perfil);
                    habilidadesToSave.Add(item);
                }

                var phAtualizadas = repository.SalvarPerfilHabilidades(habilidadesToSave);

                var habilidadesExcluir = perfil.PerfilHabilidades.Except(perfilHabilidadesNew);

                foreach (var item in habilidadesExcluir)
                {
                    item.AtribuirPerfil(perfil);
                    habilidadesToDelete.Add(item);
                }

                repository.DeletarPerfilHabilidades(habilidadesToDelete);
            }

            result.PerfilHabilidade = perfil.PerfilHabilidades.Select(ph => PerfilHabilidadeDto.Mapper(ph)).ToList();

            return result;
        }

        public CommandResult Handle(AlterarPerfilCommand command)
        {
            var result = new PerfilCommandResult();

            var perfil = GetService<IPerfilRepository>().Obter(command.Perfil.Id);

            if (perfil == null)
            {
                result.AddNotification(UsuarioMessages.PerfilNaoEcontrado);
                return result;
            }

            var perfilAlterado = command.Perfil.MapperAlterarPerfil();
            perfil.Alterar(perfilAlterado);

            if (!perfil.IsValid())
            {
                result.AddNotifications(perfil.Notifications);
                return result;
            }

            perfil = GetService<IPerfilRepository>().Alterar(perfil);

            result.Perfil = PerfilDto.Mapper(perfil);
            return result;
        }

        public CommandResult Handle(IncluirPerfilCommand command)
        {
            var result = new PerfilCommandResult();

            var usuario = GetService<IUserRepository>().Obter(command.UsuarioId);

            if (usuario == null)
            {
                result.AddNotification(UsuarioMessages.UsuarioNaoEncontrado);
                return result;
            }

            var perfilExistente = GetService<IPerfilRepository>().ObterPerfilUsuario(usuario.Id);

            if (perfilExistente != null)
            {
                result.AddNotification(UsuarioMessages.UsuarioPerfilUnico);
                return result;
            }

            var perfil = command.Perfil.MapperIncluirPerfil();

            perfil.AtribuirUsuario(usuario);
            perfil.Validar();

            if (!perfil.IsValid())
            {
                result.AddNotifications(perfil.Notifications);
                return result;
            }

            perfil = GetService<IPerfilRepository>().Salvar(perfil);

            result.Perfil = PerfilDto.Mapper(perfil);
            return result;
        }
    }
}