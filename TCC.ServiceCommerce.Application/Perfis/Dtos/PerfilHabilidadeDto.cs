﻿using TCC.ServiceCommerce.Application.Habilidades.Dtos;
using TCC.ServiceCommerce.Domain.PerfilHabilidades.Entities;

namespace TCC.ServiceCommerce.Application.Perfis.Dtos
{
    public class PerfilHabilidadeDto
    {
        public long Id { get; set; }

        public decimal ValorServico { get; set; }

        public PerfilDto Perfil { get; set; }

        public HabilidadeDto Habilidade { get; set; }

        public long HabilidadeId { get; set; }

        public static PerfilHabilidadeDto Mapper(PerfilHabilidade perfilHabilidade)
        {
            if (perfilHabilidade == null)
                return null;

            return new PerfilHabilidadeDto
            {
                Id = perfilHabilidade.Id,
                ValorServico = perfilHabilidade.ValorServico,
                HabilidadeId = perfilHabilidade.HabilidadeId,
                Habilidade = HabilidadeDto.Mapper(perfilHabilidade.Habilidade),
            };
        }

        public PerfilHabilidade MapperIncluirPerfilHabilidade()
        {
            return new PerfilHabilidade(valorServico: ValorServico,
                              habilidade: Habilidade.MapperIncluirHabilidade());
        }

        public PerfilHabilidade MapperAlterarPerfilHabilidade()
        {
            return new PerfilHabilidade(id: Id,
                              habilidade: Habilidade.MapperIncluirHabilidade(),
                              valorServico: ValorServico);
        }
    }
}
