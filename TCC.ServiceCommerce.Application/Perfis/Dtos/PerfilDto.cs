﻿using System.Collections.Generic;
using System.Linq;
using TCC.ServiceCommerce.Application.Cidades.Dtos;
using TCC.ServiceCommerce.Application.Usuarios.Dtos;
using TCC.ServiceCommerce.Domain.Perfis.Entities;

namespace TCC.ServiceCommerce.Application.Perfis.Dtos
{
    public class PerfilDto
    {
        public long Id { get; set; }

        public string NumeroDocumento { get;  set; }

        public CidadeDto Cidade { get; set; }

        public string ExperienciaProfissional { get;  set; }

        public string Sobre { get;  set; }

        public ICollection<PerfilHabilidadeDto> PerfilHabilidades { get; set; }

        public UsuarioDto Usuario { get; set; }

        public static PerfilDto Mapper(Perfil perfil)
        {
            if (perfil == null)
                return null;

            return new PerfilDto
            {
                Id = perfil.Id,
                ExperienciaProfissional = perfil.ExperienciaProfissinal,
                NumeroDocumento = perfil.NumeroDocumento,
                Sobre = perfil.Sobre,
                Cidade = CidadeDto.Mapper(perfil.Cidade),
                PerfilHabilidades = perfil.PerfilHabilidades?.Select(ph => PerfilHabilidadeDto.Mapper(ph)).ToList(),
                Usuario = UsuarioDto.Mapper(perfil.Usuario)
            };
        }

        public Perfil MapperIncluirPerfil()
        {
            return new Perfil(numeroDocumento: NumeroDocumento,
                              cidade: Cidade.MapperIncluirCidade(),
                              experienciaProfissional: ExperienciaProfissional,
                              sobre: Sobre,
                              perfilHabilidade: PerfilHabilidades?.Select(ph => ph.MapperIncluirPerfilHabilidade()).ToList());
        }

        public Perfil MapperAlterarPerfil()
        {
            return new Perfil(id: Id,
                              numeroDocumento: NumeroDocumento,
                              cidade: Cidade.MapperAlterarCidade(),
                              experienciaProfissional: ExperienciaProfissional,
                              sobre: Sobre,
                              perfilHabilidade: PerfilHabilidades?.Select(ph => ph.MapperAlterarPerfilHabilidade()).ToList());
        }
    }
}
