﻿using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Perfis.Dtos;

namespace TCC.ServiceCommerce.Application.Perfis.Commands.Results
{
    public class PerfilCommandResult : CommandResult
    {
        public PerfilDto Perfil { get; set; }
    }
}
