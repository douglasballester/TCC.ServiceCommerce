﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Perfis.Dtos;

namespace TCC.ServiceCommerce.Application.Perfis.Commands.Results
{
    public class PerfilHabilidadeCommandResult : CommandResult
    {
        public ICollection<PerfilHabilidadeDto> PerfilHabilidade { get; set; }
    }
}
