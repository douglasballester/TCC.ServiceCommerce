﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Perfis.Commands.Inputs
{
    public class DeletarPerfilHabilidadeCommand : Command
    {
        public long PerfilId { get; set; }

        public long HabilidadeId { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
