﻿using System.Collections.Generic;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Perfis.Dtos;

namespace TCC.ServiceCommerce.Application.Perfis.Commands.Inputs
{
    public class AlterarPerfilHabilidadeCommand : Command
    {
        public long PerfilId { get; set; }

        public ICollection<PerfilHabilidadeDto> PerfilHabilidades { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
