﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Perfis.Commands.Inputs
{
    public class ObterPerfilUsuarioCommand : Command
    {
        public long UsuarioId { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
