﻿using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Perfis.Dtos;

namespace TCC.ServiceCommerce.Application.Perfis.Commands.Inputs
{
    public class IncluirPerfilCommand : Command
    {
        public long UsuarioId { get; set; }

        public PerfilDto Perfil { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
