﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Perfis.Commands.Inputs
{
    public class ListarPerfilPorHabilidadeCommand : Command
    {
        public long Id { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
