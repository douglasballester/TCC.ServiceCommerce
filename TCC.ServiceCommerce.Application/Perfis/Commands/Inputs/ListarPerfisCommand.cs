﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Perfis.Commands.Inputs
{
    public class ListarPerfisCommand : Command
    {
        public override bool Validate()
        {
            return IsValid();
        }
    }
}
