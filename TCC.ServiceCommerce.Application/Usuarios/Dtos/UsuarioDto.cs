﻿using TCC.ServiceCommerce.Application.Perfis.Dtos;
using TCC.ServiceCommerce.Domain.Usuarios.Entities;

namespace TCC.ServiceCommerce.Application.Usuarios.Dtos
{
    public class UsuarioDto
    {
        public long Id { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string Senha { get; set; }

        public long PerfilId { get; set; }

        public PerfilDto Perfil { get; set; }

        public static UsuarioDto Mapper(Usuario usuario)
        {
            if (usuario == null)
                return null;

            return new UsuarioDto
            {
                Id = usuario.Id,
                Nome = usuario.Nome,
                Email = usuario.Email,
                //Perfil = PerfilDto.Mapper(usuario.Perfil)
            };
        }

        public Usuario MapperIncluirUsuario()
        {
            return new Usuario(email: Email,
                               senha: Senha,
                               nome: Nome,
                               ativo: true);
        }

        public Usuario MapperAlterarUsuario()
        {
            return new Usuario(id: Id,
                               email: Email,
                               senha: Senha,
                               nome: Nome,
                               ativo: true);
        }
    }
}
