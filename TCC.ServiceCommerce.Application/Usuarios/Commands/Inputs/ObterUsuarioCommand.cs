﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Usuarios.Commands.Inputs
{
    public class ObterUsuarioCommand : Command
    {
        public long Id { get; set; }

        public string Email { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
