﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Usuarios.Commands.Inputs
{
    public class ObterUsuarioEmailCommand : Command
    {
        public string Email { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
