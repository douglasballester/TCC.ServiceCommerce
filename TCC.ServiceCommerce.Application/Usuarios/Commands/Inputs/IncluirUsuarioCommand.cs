﻿using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Usuarios.Dtos;

namespace TCC.ServiceCommerce.Application.Usuarios.Commands.Inputs
{
    public class IncluirUsuarioCommand : Command
    {
        public UsuarioDto Usuario { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
