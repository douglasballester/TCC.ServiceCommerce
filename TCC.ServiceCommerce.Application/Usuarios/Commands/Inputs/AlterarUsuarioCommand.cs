﻿using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Usuarios.Dtos;

namespace TCC.ServiceCommerce.Application.Usuarios.Commands.Inputs
{
    public class AlterarUsuarioCommand : Command
    {
        public UsuarioDto Usuario { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
