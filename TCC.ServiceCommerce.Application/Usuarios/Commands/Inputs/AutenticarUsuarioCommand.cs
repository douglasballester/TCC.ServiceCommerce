﻿using TCC.ServiceCommerce.Application.CommandsBase;

namespace TCC.ServiceCommerce.Application.Usuarios.Commands.Inputs
{
    public class AutenticarUsuarioCommand : Command
    {
        public string Email { get; set; }

        public string Senha { get; set; }

        public override bool Validate()
        {
            return IsValid();
        }
    }
}
