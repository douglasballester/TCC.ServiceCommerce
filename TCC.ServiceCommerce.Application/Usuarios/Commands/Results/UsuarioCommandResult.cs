﻿using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Usuarios.Dtos;

namespace TCC.ServiceCommerce.Application.Usuarios.Commands.Results
{
    public class UsuarioCommandResult : CommandResult
    {
        public UsuarioDto Usuario { get; set; }
    }
}
