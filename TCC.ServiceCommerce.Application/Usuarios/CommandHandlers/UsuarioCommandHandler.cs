﻿using TCC.ServiceCommerce.Application.CommandHandlersBase;
using TCC.ServiceCommerce.Application.CommandsBase;
using TCC.ServiceCommerce.Application.Usuarios.Commands.Inputs;
using TCC.ServiceCommerce.Application.Usuarios.Commands.Results;
using TCC.ServiceCommerce.Application.Usuarios.Dtos;
using TCC.ServiceCommerce.Commom.Validation;
using TCC.ServiceCommerce.Domain.Usuarios.Messages;
using TCC.ServiceCommerce.Domain.Usuarios.Repositories;

namespace TCC.ServiceCommerce.Application.Usuarios.CommandHandlers
{
    public class UsuarioCommandHandler : CommandHandlerBase,
                                         ICommandHandler<IncluirUsuarioCommand>,
                                         ICommandHandler<ObterUsuarioCommand>,
                                         ICommandHandler<AlterarUsuarioCommand>,
                                         ICommandHandler<AutenticarUsuarioCommand>,
                                         ICommandHandler<ObterUsuarioEmailCommand>
    {
        public CommandResult Handle(AlterarUsuarioCommand command)
        {
            var result = new UsuarioCommandResult();
            var repository = GetService<IUserRepository>();

            var usuario = repository.Obter(command.Usuario.Id);
            var usuarioAlterado = command.Usuario.MapperAlterarUsuario();

            usuario.Alterar(usuarioAlterado);

            if (!usuario.IsValid())
            {
                result.AddNotifications(usuario.Notifications);
                return result;
            }

            repository.Alterar(usuario);
            result.Usuario = UsuarioDto.Mapper(usuario);

            return result;
        }

        public CommandResult Handle(ObterUsuarioEmailCommand command)
        {
            var result = new UsuarioCommandResult();

            var usuario = GetService<IUserRepository>().Obter(command.Email);

            if (usuario == null)
            {
                result.AddNotification(UsuarioMessages.UsuarioNaoEncontrado);
                return result;
            }

            result.Usuario = UsuarioDto.Mapper(usuario);

            return result;
        }

        public CommandResult Handle(AutenticarUsuarioCommand command)
        {
            var result = new UsuarioCommandResult();
            var user = GetService<IUserRepository>().Obter(command.Email);

            if (user == null || user.Senha != PasswordAssertionConcern.Encrypt(command.Senha))
            {
                result.AddNotification(UsuarioMessages.CredenciaisInvalidas);
                return result;
            }

            result.Usuario = UsuarioDto.Mapper(user);

            return result;
        }

        public CommandResult Handle(ObterUsuarioCommand command)
        {
            var result = new UsuarioCommandResult();

            var usuario = GetService<IUserRepository>().Obter(command.Id, command.Email);

            if (usuario == null)
            {
                result.AddNotification(UsuarioMessages.UsuarioNaoEncontrado);
                return result;
            }

            result.Usuario = UsuarioDto.Mapper(usuario);

            return result;
        }

        public CommandResult Handle(IncluirUsuarioCommand command)
        {
            var result = new UsuarioCommandResult();

            var usuario = command.Usuario.MapperIncluirUsuario();

            usuario.Validar();

            if (!usuario.IsValid())
            {
                result.AddNotifications(usuario.Notifications);
                return result;
            }

            usuario = GetService<IUserRepository>().Salvar(usuario);

            result.Usuario = UsuarioDto.Mapper(usuario);
            return result;
        }
    }
}
