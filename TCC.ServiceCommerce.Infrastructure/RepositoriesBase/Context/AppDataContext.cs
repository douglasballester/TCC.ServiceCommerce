﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using TCC.ServiceCommerce.Domain.Cidades.Entities;
using TCC.ServiceCommerce.Domain.Dicionarios.Entities;
using TCC.ServiceCommerce.Domain.Habilidades.Entities;
using TCC.ServiceCommerce.Domain.HistoricosContratacao.Entities;
using TCC.ServiceCommerce.Domain.Mensagens.Entities;
using TCC.ServiceCommerce.Domain.Perfis.Entities;
using TCC.ServiceCommerce.Domain.Posts.Entities;
using TCC.ServiceCommerce.Domain.Usuarios.Entities;

namespace TCC.ServiceCommerce.Infrastructure.Data.RepositoriesBase.Context
{
    public class AppDataContext : DbContext
    {
        public AppDataContext() : base("AppConnectionString")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Perfil> Perfil { get; set; }
        public DbSet<Habilidade> Habilidades { get; set; }
        public DbSet<InformacaoBancaria> InformacoesBancarias { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Mensagem> Mensagens { get; set; }
        public DbSet<Cidade> Cidades { get; set; }
        public DbSet<Dicionario> Dicionarios { get; set; }
        public DbSet<HistoricoContratacao> HistoricoContratacao { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Perfil>()
                                .HasRequired(p => p.Usuario);

            modelBuilder.Entity<InformacaoBancaria>()
                                .HasRequired(ib => ib.Usuario);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
