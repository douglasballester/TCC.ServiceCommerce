﻿using System;
using TCC.ServiceCommerce.Infrastructure.Data.RepositoriesBase.Context;

namespace TCC.ServiceCommerce.Infrastructure.RepositoriesBase
{

    public abstract class RepositoryBaseEntityFramework : IDisposable
    {
        protected AppDataContext _context;

        public RepositoryBaseEntityFramework(AppDataContext context)
        {
            _context = context;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}


