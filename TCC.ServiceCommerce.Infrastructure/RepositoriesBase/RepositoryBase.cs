﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TCC.ServiceCommerce.Commom.DI;
using TCC.ServiceCommerce.Infrastructure.Repositories.RepositoriesBase.Context;

namespace TCC.ServiceCommerce.Infrastructure.RepositoriesBase
{

    public abstract class RepositoryBase
    {
        private readonly DataBaseContext _context;
        private const int DefaultPageSize = 10;

        private class DontMap { };
        private static readonly Type TypeOfDontMap = typeof(DontMap);

        private static bool? logQuerys;

        public RepositoryBase(DataBaseContext dbContext)
        {
            _context = dbContext;
        }

        protected TService GetService<TService>() => DIContainer.Container.GetService<TService>();

        protected T ExecuteScalar<T>(string sql, object param = null)
        {
            return _context.Connection.ExecuteScalar<T>(sql, param, transaction: _context.Transaction, commandType: CommandType.Text);
        }

        protected void ExecuteNonQuery(string sql, object param = null)
        {
            _context.Connection.Execute(sql, param, transaction: _context.Transaction, commandType: CommandType.Text);
        }

        protected IDataReader ExecuteReader(string sql, object param = null)
        {
            return _context.Connection.ExecuteReader(sql, param, transaction: _context.Transaction, commandType: CommandType.Text);
        }

        protected ICollection<T> Query<T>(string sql, object param = null)
        {
            return _context.Connection.Query<T>(sql, param, transaction: _context.Transaction, commandType: CommandType.Text).ToList();
        }

        protected ICollection<TReturn> Query<TFirst, TSecond, TReturn>(string sql, Func<TFirst, TSecond, TReturn> map, object param = null)
        {
            return _context.Connection.Query(sql, map, param, transaction: _context.Transaction, commandType: CommandType.Text).ToList();
        }

        protected ICollection<TReturn> Query<TFirst, TSecond, TThird, TReturn>(string sql, Func<TFirst, TSecond, TThird, TReturn> map, object param = null)
        {
            return _context.Connection.Query(sql, map, param, transaction: _context.Transaction, commandType: CommandType.Text).ToList();
        }

        protected ICollection<TReturn> Query<TFirst, TSecond, TThird, TFourth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TReturn> map, object param = null)
        {
            return _context.Connection.Query(sql, map, param, transaction: _context.Transaction, commandType: CommandType.Text).ToList();
        }

        protected ICollection<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, object param = null)
        {
            return _context.Connection.Query(sql, map, param, transaction: _context.Transaction, commandType: CommandType.Text).ToList();
        }

        protected ICollection<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> map, object param = null)
        {
            return _context.Connection.Query(sql, map, param, transaction: _context.Transaction, commandType: CommandType.Text).ToList();
        }

        protected ICollection<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn> map, object param = null)
        {
            return _context.Connection.Query(sql, map, param, transaction: _context.Transaction, commandType: CommandType.Text).ToList();
        }

        protected ICollection<TAbstract> QueryAbstract<TAbstract>(string sql, IDictionary<int, Type> types, object param = null) where TAbstract : class
            => QueryAbstract<TAbstract>(sql, DefaultTypeDiscriminator, num => types[num], param);

        protected ICollection<TAbstract> QueryAbstract<TAbstract>(string sql, Func<IDataReader, int> discriminator, IDictionary<int, Type> types, object param = null) where TAbstract : class
            => QueryAbstract<TAbstract>(sql, discriminator, num => types[num], param);

        protected ICollection<TAbstract> QueryAbstract<TAbstract, TSecond>(string sql,
                                                                           Func<IDataReader, int> discriminator,
                                                                           Func<int, Type> typeResolver,
                                                                           Func<TAbstract, TSecond, TAbstract> map,
                                                                           string splitOn = "Id",
                                                                           object param = null)
           where TAbstract : class
            => QueryAbstract<TAbstract, TSecond, DontMap, DontMap, DontMap, DontMap, DontMap>(sql, discriminator, typeResolver, map, splitOn, param);

        protected ICollection<TAbstract> QueryAbstract<TAbstract, TSecond>(string sql,
                                                                           Func<int, Type> typeResolver,
                                                                           Func<TAbstract, TSecond, TAbstract> map,
                                                                           string splitOn = "Id",
                                                                           object param = null)
           where TAbstract : class
            => QueryAbstract<TAbstract, TSecond, DontMap, DontMap, DontMap, DontMap, DontMap>(sql, DefaultTypeDiscriminator, typeResolver, map, splitOn, param);

        protected ICollection<TAbstract> QueryAbstract<TAbstract, TSecond, TThird>(string sql,
                                                                                            Func<IDataReader, int> discriminator,
                                                                                            Func<int, Type> typeResolver,
                                                                                            Func<TAbstract, TSecond, TThird, TAbstract> map,
                                                                                            string splitOn = "Id",
                                                                                            object param = null)
            where TAbstract : class
             => QueryAbstract<TAbstract, TSecond, TThird, DontMap, DontMap, DontMap, DontMap>(sql, discriminator, typeResolver, map, splitOn, param);

        protected ICollection<TAbstract> QueryAbstract<TAbstract, TSecond, TThird>(string sql,
                                                                                          Func<int, Type> typeResolver,
                                                                                          Func<TAbstract, TSecond, TThird, TAbstract> map,
                                                                                          string splitOn = "Id",
                                                                                          object param = null)
          where TAbstract : class
           => QueryAbstract<TAbstract, TSecond, TThird, DontMap, DontMap, DontMap, DontMap>(sql, DefaultTypeDiscriminator, typeResolver, map, splitOn, param);

        protected ICollection<TAbstract> QueryAbstract<TAbstract, TSecond, TThird, TFourth>(string sql,
                                                                                           Func<IDataReader, int> discriminator,
                                                                                           Func<int, Type> typeResolver,
                                                                                           Func<TAbstract, TSecond, TThird, TFourth, TAbstract> map,
                                                                                           string splitOn = "Id",
                                                                                           object param = null)
           where TAbstract : class
            => QueryAbstract<TAbstract, TSecond, TThird, TFourth, DontMap, DontMap, DontMap>(sql, discriminator, typeResolver, map, splitOn, param);

        protected ICollection<TAbstract> QueryAbstract<TAbstract, TSecond, TThird, TFourth>(string sql,
                                                                                           Func<int, Type> typeResolver,
                                                                                           Func<TAbstract, TSecond, TThird, TFourth, TAbstract> map,
                                                                                           string splitOn = "Id",
                                                                                           object param = null)
           where TAbstract : class
            => QueryAbstract<TAbstract, TSecond, TThird, TFourth, DontMap, DontMap, DontMap>(sql, DefaultTypeDiscriminator, typeResolver, map, splitOn, param);

        protected ICollection<TAbstract> QueryAbstract<TAbstract, TSecond, TThird, TFourth, TFifth>(string sql,
                                                                                                    Func<IDataReader, int> discriminator,
                                                                                                    Func<int, Type> typeResolver,
                                                                                                    Func<TAbstract, TSecond, TThird, TFourth, TFifth, TAbstract> map,
                                                                                                    string splitOn = "Id",
                                                                                                    object param = null)
           where TAbstract : class
            => QueryAbstract<TAbstract, TSecond, TThird, TFourth, TFifth, DontMap, DontMap>(sql, discriminator, typeResolver, map, splitOn, param);

        protected ICollection<TAbstract> QueryAbstract<TAbstract, TSecond, TThird, TFourth, TFifth>(string sql,
                                                                                                   Func<int, Type> typeResolver,
                                                                                                   Func<TAbstract, TSecond, TThird, TFourth, TFifth, TAbstract> map,
                                                                                                   string splitOn = "Id",
                                                                                                   object param = null)
          where TAbstract : class
           => QueryAbstract<TAbstract, TSecond, TThird, TFourth, TFifth, DontMap, DontMap>(sql, DefaultTypeDiscriminator, typeResolver, map, splitOn, param);

        protected ICollection<TAbstract> QueryAbstract<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth>(string sql,
                                                                                                            Func<IDataReader, int> discriminator,
                                                                                                            Func<int, Type> typeResolver,
                                                                                                            Func<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, TAbstract> map,
                                                                                                            string splitOn = "Id",
                                                                                                            object param = null)
           where TAbstract : class
            => QueryAbstract<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, DontMap>(sql, discriminator, typeResolver, map, splitOn, param);

        protected ICollection<TAbstract> QueryAbstract<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth>(string sql,
                                                                                                           Func<int, Type> typeResolver,
                                                                                                           Func<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, TAbstract> map,
                                                                                                           string splitOn = "Id",
                                                                                                           object param = null)
          where TAbstract : class
           => QueryAbstract<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, DontMap>(sql, DefaultTypeDiscriminator, typeResolver, map, splitOn, param);

        protected ICollection<TAbstract> QueryAbstract<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh>(string sql,
                                                                                                                     Func<IDataReader, int> discriminator,
                                                                                                                     Func<int, Type> typeResolver,
                                                                                                                     Func<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TAbstract> map,
                                                                                                                     string splitOn = "Id",
                                                                                                                     object param = null)
            where TAbstract : class
            => QueryAbstract<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh>(sql, discriminator, typeResolver, (Delegate)map, splitOn, param);

        protected ICollection<TAbstract> QueryAbstract<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh>(string sql,
                                                                                                                     Func<int, Type> typeResolver,
                                                                                                                     Func<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TAbstract> map,
                                                                                                                     string splitOn = "Id",
                                                                                                                     object param = null)
            where TAbstract : class
            => QueryAbstract<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh>(sql, DefaultTypeDiscriminator, typeResolver, (Delegate)map, splitOn, param);

        protected ICollection<TAbstract> QueryAbstract<TAbstract>(string sql, Func<IDataReader, int> discriminator, Func<int, Type> typeResolver, object param = null) where TAbstract : class
        {
            List<TAbstract> result = new List<TAbstract>();

            using (IDataReader reader = _context.Connection.ExecuteReader(sql, param: param, transaction: _context.Transaction))
            {
                while (reader.Read())
                {
                    result.Add((TAbstract)SqlMapper.GetRowParser(reader, typeResolver(discriminator(reader)))(reader));
                }
            }

            return result;
        }

        protected ICollection<TAbstract> QueryAbstract<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh>(string sql,
                                                                                                                      Func<IDataReader, int> discriminator,
                                                                                                                      Func<int, Type> typeResolver,
                                                                                                                      Delegate map,
                                                                                                                      string splitOn = "Id",
                                                                                                                      object param = null)
            where TAbstract : class
        {
            List<TAbstract> result = new List<TAbstract>();
            Type[] types = new Type[] { typeof(TSecond), typeof(TThird), typeof(TFourth), typeof(TSixth), typeof(TSeventh) };
            object[] rowArgs = new object[7];

            using (IDataReader reader = _context.Connection.ExecuteReader(sql, param: param, transaction: _context.Transaction))
            {
                while (reader.Read())
                {
                    Array.Clear(rowArgs, 0, 7);
                    int paramCount = 1;
                    int startIndex = 0;
                    int length = 0;

                    GetNextSplit(reader, splitOn, startIndex, ref length);
                    int currentPos = length;
                    TAbstract root = ((TAbstract)SqlMapper.GetRowParser(reader, typeResolver(discriminator(reader)), startIndex, length)(reader));

                    rowArgs[0] = root;

                    for (int i = 0; i < types.Length; i++)
                    {
                        if (types[i] == TypeOfDontMap)
                        {
                            break;
                        }

                        paramCount++;
                        startIndex = currentPos;

                        length = GetNextSplit(reader, splitOn, startIndex, ref currentPos) ?
                            currentPos - startIndex
                            :
                            reader.FieldCount - currentPos;

                        rowArgs[i + 1] = SqlMapper.GetRowParser(reader, types[i], startIndex, length)(reader);
                    }

                    GenerateMapper<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh>(rowArgs, paramCount, map)(reader);
                    result.Add(root);
                }
            }

            return result;
        }

        private bool GetNextSplit(IDataReader reader, string splitOn, int startIndex, ref int currentPos)
        {
            for (int i = startIndex + 1; i < reader.FieldCount; i++)
            {
                if (string.Equals(reader.GetName(i), splitOn, StringComparison.InvariantCultureIgnoreCase))
                {
                    currentPos = i;
                    return true;
                }
            }

            return false;
        }

        private Func<IDataReader, TAbstract> GenerateMapper<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh>(object[] rowArgs, int paramCount, Delegate map)
            where TAbstract : class
        {
            switch (paramCount)
            {
                case 2:
                    return reader => ((Func<TAbstract, TSecond, TAbstract>)map)((TAbstract)rowArgs[0], (TSecond)rowArgs[1]);

                case 3:
                    return reader => ((Func<TAbstract, TSecond, TThird, TAbstract>)map)((TAbstract)rowArgs[0], (TSecond)rowArgs[1], (TThird)rowArgs[2]);

                case 4:
                    return reader => ((Func<TAbstract, TSecond, TThird, TFourth, TAbstract>)map)((TAbstract)rowArgs[0], (TSecond)rowArgs[1], (TThird)rowArgs[2], (TFourth)rowArgs[3]);

                case 5:
                    return reader => ((Func<TAbstract, TSecond, TThird, TFourth, TFifth, TAbstract>)map)((TAbstract)rowArgs[0], (TSecond)rowArgs[1], (TThird)rowArgs[2], (TFourth)rowArgs[3], (TFifth)rowArgs[4]);

                case 6:
                    return reader => ((Func<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, TAbstract>)map)((TAbstract)rowArgs[0], (TSecond)rowArgs[1], (TThird)rowArgs[2], (TFourth)rowArgs[3], (TFifth)rowArgs[4], (TSixth)rowArgs[5]);

                case 7:
                    return reader => ((Func<TAbstract, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TAbstract>)map)((TAbstract)rowArgs[0], (TSecond)rowArgs[1], (TThird)rowArgs[2], (TFourth)rowArgs[3], (TFifth)rowArgs[4], (TSixth)rowArgs[5], (TSeventh)rowArgs[6]);

                default:
                    return null;
            }
        }

        private IEnumerable<T> ExecuteSearch<T>(string sql, out int totalRecords, object param = null)
        {
            List<T> records = new List<T>(DefaultPageSize);
            int? recordsCount = null;

            using (IDataReader reader = ExecuteReader(sql, param))
            {
                var func = SqlMapper.GetRowParser(reader, typeof(T));
                while (reader.Read())
                {
                    if (!recordsCount.HasValue)
                        recordsCount = Convert.ToInt32(reader["TotalRecords"]);

                    records.Add((T)func(reader));
                }
            }

            totalRecords = recordsCount.GetValueOrDefault();
            return records;
        }

        //protected IPagedList<T> PagedQuery<T>(string sql, int page, int pageSize, object param = null)
        //{
        //    int totalRecords;
        //    var source = ExecuteSearch<T>(sql, out totalRecords, param: param);

        //    return new PagedList<T>(source, page, pageSize, totalRecords);
        //}

        static int DefaultTypeDiscriminator(IDataReader reader) => reader.GetInt32(reader.GetOrdinal("Id"));
    }
}


