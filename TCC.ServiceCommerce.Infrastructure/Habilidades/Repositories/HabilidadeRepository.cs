﻿using System.Collections.Generic;
using System.Linq;
using TCC.ServiceCommerce.Domain.Habilidades.Entities;
using TCC.ServiceCommerce.Domain.Habilidades.Repositories;
using TCC.ServiceCommerce.Infrastructure.Data.RepositoriesBase.Context;
using TCC.ServiceCommerce.Infrastructure.RepositoriesBase;

namespace TCC.ServiceCommerce.Infrastructure.Habilidades.Repositories
{
    public class HabilidadeRepository : RepositoryBaseEntityFramework, IHabilidadeRepository
    {
        public HabilidadeRepository(AppDataContext context) : base(context)
        { }

        public Habilidade Obter(long id)
        {
            return _context.Habilidades.FirstOrDefault(h => h.Id == id);
        }

        public ICollection<Habilidade> ObterTodas()
        {
            return _context.Habilidades.ToList();
        }
    }
}
