﻿using System.Collections.Generic;
using System.Linq;
using TCC.ServiceCommerce.Domain.HistoricosContratacao.Entities;
using TCC.ServiceCommerce.Domain.HistoricosContratacao.Repositories;
using TCC.ServiceCommerce.Infrastructure.Data.RepositoriesBase.Context;
using TCC.ServiceCommerce.Infrastructure.RepositoriesBase;
using System.Data.Entity;

namespace TCC.ServiceCommerce.Infrastructure.HistoricosContratacao.Repositories
{
    public class HistoricoContratacaoRepository : RepositoryBaseEntityFramework, IHistoricoContratacaoRepository
    {
        public HistoricoContratacaoRepository(AppDataContext context) : base(context)
        { }

        public ICollection<HistoricoContratacao> ListarHistoricos()
        {
            return _context.HistoricoContratacao.Include(h => h.Contratante)
                                                .Include(h => h.Contratante.HistoricoContratacao)
                                                .Include(h => h.Contratado)
                                                .Include(h => h.ServicoContratado).ToList();
        }
    }
}
