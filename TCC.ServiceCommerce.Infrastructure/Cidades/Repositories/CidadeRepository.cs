﻿using System.Collections.Generic;
using System.Linq;
using TCC.ServiceCommerce.Domain.Cidades.Entities;
using TCC.ServiceCommerce.Domain.Cidades.Repositories;
using TCC.ServiceCommerce.Infrastructure.Data.RepositoriesBase.Context;
using TCC.ServiceCommerce.Infrastructure.RepositoriesBase;

namespace TCC.ServiceCommerce.Infrastructure.Cidades.Repositories
{
    public class CidadeRepository : RepositoryBaseEntityFramework, ICidadeRepository
    {
        public CidadeRepository(AppDataContext context) : base(context)
        { }

        public Cidade Obter(long id)
        {
            return _context.Cidades.FirstOrDefault(c => c.Id == id);
        }

        public ICollection<Cidade> ObterTodas()
        {
            return _context.Cidades.ToList();
        }
    }
}
