﻿MERGE [USUARIO] AS TARGET
USING (SELECT @Id) AS SOURCE (Id)
	ON (TARGET.Id = SOURCE.Id AND SOURCE.Id IS NOT NULL)
WHEN MATCHED THEN
	UPDATE
	SET
		[Email] = @Email,
		[Senha] = @Senha,
		[Nome] = @Nome,
		[Ativo] = @Ativo
WHEN NOT MATCHED THEN
	INSERT (
		[Email],
		[Senha],
		[Nome],
		[Ativo]
	) VALUES (
		@Email,
		@Senha,
		@Nome,
		@Ativo
	);

SELECT IIF(@Id > 0, @Id, SCOPE_IDENTITY())