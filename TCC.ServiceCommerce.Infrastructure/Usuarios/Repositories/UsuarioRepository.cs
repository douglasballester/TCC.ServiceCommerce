﻿using System.Collections.Generic;
using System.Linq;
using TCC.ServiceCommerce.Domain.Usuarios.Entities;
using TCC.ServiceCommerce.Domain.Usuarios.Repositories;
using TCC.ServiceCommerce.Infrastructure.Data.RepositoriesBase.Context;
using TCC.ServiceCommerce.Infrastructure.RepositoriesBase;

namespace TCC.ServiceCommerce.Infrastructure.Usuarios.Repositories
{
    public class UsuarioRepository : RepositoryBaseEntityFramework, IUserRepository
    {
        public UsuarioRepository(AppDataContext context) : base(context)
        { }

        public Usuario Salvar(Usuario user)
        {
            _context.Usuarios.Add(user);
            _context.SaveChanges();

            return user;
        }

        public void Alterar(Usuario user)
        {
            _context.Entry(user).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }

        public ICollection<Usuario> ListarTodos()
        {
            return _context.Usuarios.ToList();
        }

        public Usuario Obter(long id)
        {
            return Obter(id, null);
        }

        public Usuario Obter(string email)
        {
            return Obter(null, email);
        }

        public Usuario Obter(long? id, string email)
        {
            return _context.Usuarios.Where(u => u.Id == id || u.Email.Equals(email)).FirstOrDefault();
        }
    }
}
