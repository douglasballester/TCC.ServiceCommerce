﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TCC.ServiceCommerce.Domain.Posts.Entities;
using TCC.ServiceCommerce.Domain.Posts.Repositories;
using TCC.ServiceCommerce.Infrastructure.Data.RepositoriesBase.Context;
using TCC.ServiceCommerce.Infrastructure.RepositoriesBase;

namespace TCC.ServiceCommerce.Infrastructure.Posts.Repositories
{
    public class PostRepository : RepositoryBaseEntityFramework, IPostRepository
    {
        public PostRepository(AppDataContext context) : base(context)
        { }

        public void Delete(Post post)
        {
            _context.Entry(post).State = EntityState.Deleted;
            _context.SaveChanges();
        }

        public ICollection<Post> ListarPostsUsuario(long usuarioId)
        {
            return _context.Posts.Include(p => p.Mensagem)
                                 .Include(p => p.Mensagem.UserSend)
                                 .Where(p => p.UserDestination.Id == usuarioId).ToList();
        }

        public Post Obter(long id)
        {
            return _context.Posts.Include(p => p.Mensagem)
                                 .Include(p => p.UserDestination)
                                 .FirstOrDefault(p => p.Id == id);
        }

        public Post Salvar(Post post)
        {
            _context.Entry(post.Mensagem.UserSend).State = EntityState.Unchanged;
            _context.Entry(post.UserDestination).State = EntityState.Unchanged;
            _context.Posts.Add(post);
            _context.SaveChanges();
            return post;
        }
    }
}
