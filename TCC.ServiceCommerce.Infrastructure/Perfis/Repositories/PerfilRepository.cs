﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TCC.ServiceCommerce.Domain.PerfilHabilidades.Entities;
using TCC.ServiceCommerce.Domain.Perfis.Entities;
using TCC.ServiceCommerce.Domain.Perfis.Repositories;
using TCC.ServiceCommerce.Infrastructure.Data.RepositoriesBase.Context;
using TCC.ServiceCommerce.Infrastructure.RepositoriesBase;

namespace TCC.ServiceCommerce.Infrastructure.Perfis.Repositories
{
    public class PerfilRepository : RepositoryBaseEntityFramework, IPerfilRepository
    {
        public PerfilRepository(AppDataContext context) : base(context)
        { }

        public Perfil Salvar(Perfil perfil)
        {
            _context.Entry(perfil.Cidade).State = EntityState.Unchanged;
            _context.Perfil.Add(perfil);
            _context.SaveChanges();
            _context.Perfil.Include(p => p.Cidade)
                            .Include(p => p.Usuario);


            return perfil;
        }

        public Perfil Alterar(Perfil perfil)
        {
            _context.Entry(perfil.Cidade).State = EntityState.Unchanged;
            foreach (var perfilhab in perfil.PerfilHabilidades)
            {
                _context.Entry(perfilhab).State = EntityState.Unchanged;
            }

            _context.Entry(perfil).State = EntityState.Modified;
            _context.Perfil.Include(p => p.Usuario)
                            .Include(p => p.CidadeId)
                            .Include(p => p.Cidade);

            _context.SaveChanges();
          
            return perfil;
        }

        public Perfil Obter(string numeroDocumento)
        {
            return _context.Perfil.Where(p => p.NumeroDocumento == numeroDocumento).FirstOrDefault();
        }

        public Perfil Obter(long id)
        {
            return _context.Perfil.Include(p => p.Cidade)
                                  .Include(p => p.PerfilHabilidades.Select(ph => ph.Habilidade))
                                  .Include(p => p.Usuario)
                                  .Where(p => p.Id == id).FirstOrDefault();
        }

        public Perfil SalvarPerfilHabilidades(Perfil perfil)
        {
            _context.Entry(perfil.Cidade).State = EntityState.Unchanged;

            foreach (var perfilhab in perfil.PerfilHabilidades)
            {
                _context.Entry(perfilhab).State = EntityState.Added;
                _context.Entry(perfilhab.Habilidade).State = EntityState.Unchanged;
                _context.Entry(perfilhab.Perfil).State = EntityState.Unchanged;
            }

            _context.Entry(perfil).State = EntityState.Unchanged;
            _context.SaveChanges();

            return perfil;
        }

        public Perfil AlterarPerfilHabilidades(Perfil perfil)
        {
            foreach (var perfilhab in perfil.PerfilHabilidades)
            {
                _context.Entry(perfilhab).State = EntityState.Modified;
            }

            _context.SaveChanges();

            return perfil;
        }

        public Perfil ObterPerfilUsuario(long usuarioId)
        {
            return _context.Perfil.Include(p => p.Usuario)
                                  .Include(p => p.Cidade)
                                  .Include(p => p.PerfilHabilidades)
                                  .Where(p => p.UsuarioId == usuarioId).FirstOrDefault();
        }

        public ICollection<Perfil> ListarPerfis()
        {
            return _context.Perfil.Include(p => p.Usuario)
                                  .Include(p => p.Cidade)
                                  .Include(p => p.PerfilHabilidades.Select(ph => ph.Habilidade))
                                  .ToList();
        }

        public ICollection<PerfilHabilidade> DeletarPerfilHabilidades(ICollection<PerfilHabilidade> perfilHabilidades)
        {
            foreach (var ph in perfilHabilidades)
            {
                _context.Entry(ph.Habilidade).State = EntityState.Unchanged;
                _context.Entry(ph.Perfil).State = EntityState.Unchanged;
                _context.Entry(ph).State = EntityState.Deleted;
            }

            _context.SaveChanges();

            return perfilHabilidades;
        }

        public ICollection<PerfilHabilidade> SalvarPerfilHabilidades(ICollection<PerfilHabilidade> perfilHabilidades)
        {
            foreach (var perfilhab in perfilHabilidades)
            {
                _context.Entry(perfilhab).State = EntityState.Added;
                _context.Entry(perfilhab.Habilidade).State = EntityState.Unchanged;
                _context.Entry(perfilhab.Perfil).State = EntityState.Unchanged;
            }

            _context.SaveChanges();

            return perfilHabilidades;
        }
    }
}
