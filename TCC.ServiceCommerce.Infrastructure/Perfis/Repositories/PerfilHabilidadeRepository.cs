﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using TCC.ServiceCommerce.Domain.PerfilHabilidades.Entities;
using TCC.ServiceCommerce.Domain.PerfilHabilidades.Repositories;
using TCC.ServiceCommerce.Domain.Perfis.Entities;
using TCC.ServiceCommerce.Infrastructure.Data.RepositoriesBase.Context;
using TCC.ServiceCommerce.Infrastructure.RepositoriesBase;

namespace TCC.ServiceCommerce.Infrastructure.Perfis.Repositories
{
    public class PerfilHabilidadeRepository : RepositoryBaseEntityFramework, IPerfilHabilidadeRepository
    {
        public PerfilHabilidadeRepository(AppDataContext context) : base(context)
        { }

        public void Alterar(ICollection<PerfilHabilidade> perfilHabilidade)
        {
            throw new NotImplementedException();
        }

        public ICollection<PerfilHabilidade> Obter(long perfilId)
        {
            throw new NotImplementedException();
        }

        public ICollection<PerfilHabilidade> Salvar(ICollection<PerfilHabilidade> perfilHabilidade)
        {
            //_context.Entry(perfil.Cidade).State = EntityState.Unchanged;
            //_context.Perfil.Add(perfil);
            //_context.SaveChanges();

            //return perfil;

            return null;
        }

        public Perfil Salvar(Perfil perfil)
        {
            _context.Entry(perfil.Cidade).State = EntityState.Unchanged;
            _context.Perfil.Add(perfil);
            _context.SaveChanges();

            return perfil;
        }
    }
}
