namespace TCC.ServiceCommerce.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HistoricoContratacao : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HistoricoContratacao",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ContratanteId = c.Long(nullable: false),
                        ContratadoId = c.Long(nullable: false),
                        Usuario_Id = c.Long(),
                        ServicoContratado_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Usuario", t => t.Usuario_Id)
                .ForeignKey("dbo.Usuario", t => t.ContratadoId, cascadeDelete: false)
                .ForeignKey("dbo.Usuario", t => t.ContratanteId, cascadeDelete: false)
                .ForeignKey("dbo.Habilidade", t => t.ServicoContratado_Id)
                .Index(t => t.ContratanteId)
                .Index(t => t.ContratadoId)
                .Index(t => t.Usuario_Id)
                .Index(t => t.ServicoContratado_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HistoricoContratacao", "ServicoContratado_Id", "dbo.Habilidade");
            DropForeignKey("dbo.HistoricoContratacao", "ContratanteId", "dbo.Usuario");
            DropForeignKey("dbo.HistoricoContratacao", "ContratadoId", "dbo.Usuario");
            DropForeignKey("dbo.HistoricoContratacao", "Usuario_Id", "dbo.Usuario");
            DropIndex("dbo.HistoricoContratacao", new[] { "ServicoContratado_Id" });
            DropIndex("dbo.HistoricoContratacao", new[] { "Usuario_Id" });
            DropIndex("dbo.HistoricoContratacao", new[] { "ContratadoId" });
            DropIndex("dbo.HistoricoContratacao", new[] { "ContratanteId" });
            DropTable("dbo.HistoricoContratacao");
        }
    }
}
