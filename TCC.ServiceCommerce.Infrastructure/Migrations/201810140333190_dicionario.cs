namespace TCC.ServiceCommerce.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dicionario : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dicionario",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Palavra = c.String(),
                        Fonetizar = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Dicionario");
        }
    }
}
