using System.Data.Entity.Migrations;
using TCC.ServiceCommerce.Infrastructure.Data.RepositoriesBase.Context;

namespace TCC.ServiceCommerce.Infrastructure.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<AppDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AppDataContext context)
        {
            //var cidades = new List<Cidade>()
            //{
            //    new Cidade("Cachoeirinha"),
            //    new Cidade("Alvorada"),
            //    new Cidade("Campo Bom"),
            //    new Cidade("Canoas"),
            //    new Cidade("Estancia Velha"),
            //    new Cidade("Esteio"),
            //    new Cidade("Gravata�"),
            //    new Cidade("Gua�ba"),
            //    new Cidade("Novo Hamburgo"),
            //    new Cidade("Porto Alegre"),
            //    new Cidade("S�o Leopoldo"),
            //    new Cidade("S�o Sebasti�o do Ca�"),
            //    new Cidade("Sapiranga"),
            //    new Cidade("Sapucaia do Sul"),
            //    new Cidade("Viam�o"),
            //    new Cidade("Eldorado do Sul"),
            //    new Cidade("Glorinha"),
            //    new Cidade("Dois Irm�os"),
            //    new Cidade("Ivoti"),
            //    new Cidade("Nova Hartz"),
            //    new Cidade("Parob�"),
            //    new Cidade("Port�o"),
            //    new Cidade("Triunfo"),
            //    new Cidade("Charqueadas"),
            //    new Cidade("Araric�"),
            //    new Cidade("MonteNegro"),
            //    new Cidade("Taquara"),
            //    new Cidade("S�o Jer�nimo"),
            //    new Cidade("Arroio dos Ratos"),
            //    new Cidade("Santo Ant�nio da Patrulha"),
            //    new Cidade("Capela de Santana"),
            //    new Cidade("Rolante"),
            //    new Cidade("Igrejinha"),
            //    new Cidade("Nova Santa Rita")
            //};
            //context.Cidades.AddOrUpdate();
        }
    }
}
