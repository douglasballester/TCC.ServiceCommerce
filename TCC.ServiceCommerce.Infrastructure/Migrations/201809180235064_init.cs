namespace TCC.ServiceCommerce.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cidade",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Descricao = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Habilidade",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Descricao = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.InformacaoBancaria",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        UsuarioId = c.Long(nullable: false),
                        Agencia = c.String(),
                        Conta = c.String(),
                        Dv = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Usuario", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Usuario",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Email = c.String(),
                        Senha = c.String(),
                        Nome = c.String(),
                        Ativo = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Perfil",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        UsuarioId = c.Long(nullable: false),
                        NumeroDocumento = c.String(),
                        CidadeId = c.Long(nullable: false),
                        ExperienciaProfissinal = c.String(),
                        Sobre = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cidade", t => t.CidadeId, cascadeDelete: true)
                .ForeignKey("dbo.Usuario", t => t.Id)
                .Index(t => t.Id)
                .Index(t => t.CidadeId);
            
            CreateTable(
                "dbo.PerfilHabilidade",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ValorServico = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HabilidadeId = c.Long(nullable: false),
                        Perfil_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Habilidade", t => t.HabilidadeId, cascadeDelete: true)
                .ForeignKey("dbo.Perfil", t => t.Perfil_Id)
                .Index(t => t.HabilidadeId)
                .Index(t => t.Perfil_Id);
            
            CreateTable(
                "dbo.Post",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserDestinationId = c.Long(nullable: false),
                        Mensagem_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Mensagem", t => t.Mensagem_Id)
                .ForeignKey("dbo.Usuario", t => t.UserDestinationId, cascadeDelete: true)
                .Index(t => t.UserDestinationId)
                .Index(t => t.Mensagem_Id);
            
            CreateTable(
                "dbo.Mensagem",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Descricao = c.String(),
                        UserSendId = c.Long(nullable: false),
                        DataPostagem = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Usuario", t => t.UserSendId, cascadeDelete: true)
                .Index(t => t.UserSendId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InformacaoBancaria", "Id", "dbo.Usuario");
            DropForeignKey("dbo.Post", "UserDestinationId", "dbo.Usuario");
            DropForeignKey("dbo.Post", "Mensagem_Id", "dbo.Mensagem");
            DropForeignKey("dbo.Mensagem", "UserSendId", "dbo.Usuario");
            DropForeignKey("dbo.Perfil", "Id", "dbo.Usuario");
            DropForeignKey("dbo.PerfilHabilidade", "Perfil_Id", "dbo.Perfil");
            DropForeignKey("dbo.PerfilHabilidade", "HabilidadeId", "dbo.Habilidade");
            DropForeignKey("dbo.Perfil", "CidadeId", "dbo.Cidade");
            DropIndex("dbo.Mensagem", new[] { "UserSendId" });
            DropIndex("dbo.Post", new[] { "Mensagem_Id" });
            DropIndex("dbo.Post", new[] { "UserDestinationId" });
            DropIndex("dbo.PerfilHabilidade", new[] { "Perfil_Id" });
            DropIndex("dbo.PerfilHabilidade", new[] { "HabilidadeId" });
            DropIndex("dbo.Perfil", new[] { "CidadeId" });
            DropIndex("dbo.Perfil", new[] { "Id" });
            DropIndex("dbo.InformacaoBancaria", new[] { "Id" });
            DropTable("dbo.Mensagem");
            DropTable("dbo.Post");
            DropTable("dbo.PerfilHabilidade");
            DropTable("dbo.Perfil");
            DropTable("dbo.Usuario");
            DropTable("dbo.InformacaoBancaria");
            DropTable("dbo.Habilidade");
            DropTable("dbo.Cidade");
        }
    }
}
