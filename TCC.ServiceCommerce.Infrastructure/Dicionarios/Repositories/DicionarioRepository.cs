﻿using System.Collections.Generic;
using System.Linq;
using TCC.ServiceCommerce.Domain.Dicionarios.Entities;
using TCC.ServiceCommerce.Domain.Dicionarios.Repositories;
using TCC.ServiceCommerce.Infrastructure.Data.RepositoriesBase.Context;
using TCC.ServiceCommerce.Infrastructure.RepositoriesBase;

namespace TCC.ServiceCommerce.Infrastructure.Dicionarios.Repositories
{
    public class DicionarioRepository : RepositoryBaseEntityFramework, IDicionarioRepository
    {
        public DicionarioRepository(AppDataContext context) : base(context)
        { }

        public ICollection<Dicionario> ListarPalavras()
        {
            return _context.Dicionarios.ToList();
        }
    }
}
