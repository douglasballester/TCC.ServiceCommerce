﻿using System.Data.Entity;
using System.Linq;
using TCC.ServiceCommerce.Domain.Mensagens.Entities;
using TCC.ServiceCommerce.Domain.Mensagens.Repositories;
using TCC.ServiceCommerce.Infrastructure.Data.RepositoriesBase.Context;
using TCC.ServiceCommerce.Infrastructure.RepositoriesBase;

namespace TCC.ServiceCommerce.Infrastructure.Mensagens.Repositories
{
    public class MensagemRepository : RepositoryBaseEntityFramework, IMensagemRepository
    {
        public MensagemRepository(AppDataContext context) : base(context)
        { }

        public void Delete(Mensagem mensagem)
        {
            _context.Entry(mensagem).State = EntityState.Deleted;
            _context.SaveChanges();
        }

        public Mensagem Obter(long id)
        {
            return _context.Mensagens.FirstOrDefault(m => m.Id == id);
        }

        public Mensagem Salvar(Mensagem mensagem)
        {
            _context.Mensagens.Add(mensagem);
            _context.SaveChanges();
            return mensagem;
        }
    }
}
